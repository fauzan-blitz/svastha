// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BannerAdapter$BannerViewHolder_ViewBinding implements Unbinder {
  private BannerAdapter.BannerViewHolder target;

  @UiThread
  public BannerAdapter$BannerViewHolder_ViewBinding(BannerAdapter.BannerViewHolder target,
      View source) {
    this.target = target;

    target.ivBanner = Utils.findRequiredViewAsType(source, R.id.iv_banner, "field 'ivBanner'", ImageView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BannerAdapter.BannerViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivBanner = null;
  }
}
