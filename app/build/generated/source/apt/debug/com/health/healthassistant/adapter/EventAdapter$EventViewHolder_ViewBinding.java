// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EventAdapter$EventViewHolder_ViewBinding implements Unbinder {
  private EventAdapter.EventViewHolder target;

  @UiThread
  public EventAdapter$EventViewHolder_ViewBinding(EventAdapter.EventViewHolder target,
      View source) {
    this.target = target;

    target.ivFeeds = Utils.findRequiredViewAsType(source, R.id.iv_feeds, "field 'ivFeeds'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", TextView.class);
    target.tvCategory = Utils.findRequiredViewAsType(source, R.id.tv_category, "field 'tvCategory'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EventAdapter.EventViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivFeeds = null;
    target.tvTitle = null;
    target.tvCategory = null;
  }
}
