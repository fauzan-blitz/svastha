// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.view.feeds;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class FeedsFragment_ViewBinding implements Unbinder {
  private FeedsFragment target;

  @UiThread
  public FeedsFragment_ViewBinding(FeedsFragment target, View source) {
    this.target = target;

    target.rvRecommendation = Utils.findRequiredViewAsType(source, R.id.rv_recommendation, "field 'rvRecommendation'", RecyclerView.class);
    target.indicatorRecommentdation = Utils.findRequiredViewAsType(source, R.id.indicator_recommentdation, "field 'indicatorRecommentdation'", IndefinitePagerIndicator.class);
    target.rvRecommendationBeverage = Utils.findRequiredViewAsType(source, R.id.rv_recommendation_beverage, "field 'rvRecommendationBeverage'", RecyclerView.class);
    target.indicatorRecommentdationBeverage = Utils.findRequiredViewAsType(source, R.id.indicator_recommentdation_beverage, "field 'indicatorRecommentdationBeverage'", IndefinitePagerIndicator.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    FeedsFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvRecommendation = null;
    target.indicatorRecommentdation = null;
    target.rvRecommendationBeverage = null;
    target.indicatorRecommentdationBeverage = null;
  }
}
