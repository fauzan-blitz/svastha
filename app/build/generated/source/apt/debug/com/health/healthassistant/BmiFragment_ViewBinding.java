// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class BmiFragment_ViewBinding implements Unbinder {
  private BmiFragment target;

  @UiThread
  public BmiFragment_ViewBinding(BmiFragment target, View source) {
    this.target = target;

    target.tvIbmScore = Utils.findRequiredViewAsType(source, R.id.tv_ibm_score, "field 'tvIbmScore'", TextView.class);
    target.edtHeight = Utils.findRequiredViewAsType(source, R.id.edt_height, "field 'edtHeight'", EditText.class);
    target.edtWeight = Utils.findRequiredViewAsType(source, R.id.edt_weight1, "field 'edtWeight'", EditText.class);
    target.edtAge = Utils.findRequiredViewAsType(source, R.id.edt_age, "field 'edtAge'", EditText.class);
    target.btnCount = Utils.findRequiredViewAsType(source, R.id.btn_count, "field 'btnCount'", Button.class);
    target.tvItmStatus = Utils.findRequiredViewAsType(source, R.id.tv_itm_status, "field 'tvItmStatus'", TextView.class);
    target.rbMale = Utils.findRequiredViewAsType(source, R.id.rb_male, "field 'rbMale'", RadioButton.class);
    target.rbFemale = Utils.findRequiredViewAsType(source, R.id.rb_female, "field 'rbFemale'", RadioButton.class);
    target.rgGender = Utils.findRequiredViewAsType(source, R.id.rg_gender, "field 'rgGender'", RadioGroup.class);
    target.tvWeight = Utils.findRequiredViewAsType(source, R.id.tv_weight, "field 'tvWeight'", TextView.class);
    target.tvHeight = Utils.findRequiredViewAsType(source, R.id.tv_height, "field 'tvHeight'", TextView.class);
    target.tvCountScore = Utils.findRequiredViewAsType(source, R.id.tv_count_score, "field 'tvCountScore'", TextView.class);
    target.viewBmiFormula = Utils.findRequiredViewAsType(source, R.id.view_bmi_formula, "field 'viewBmiFormula'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    BmiFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvIbmScore = null;
    target.edtHeight = null;
    target.edtWeight = null;
    target.edtAge = null;
    target.btnCount = null;
    target.tvItmStatus = null;
    target.rbMale = null;
    target.rbFemale = null;
    target.rgGender = null;
    target.tvWeight = null;
    target.tvHeight = null;
    target.tvCountScore = null;
    target.viewBmiFormula = null;
  }
}
