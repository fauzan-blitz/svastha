// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.view.home;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeFragment_ViewBinding implements Unbinder {
  private HomeFragment target;

  @UiThread
  public HomeFragment_ViewBinding(HomeFragment target, View source) {
    this.target = target;

    target.rvBanner = Utils.findRequiredViewAsType(source, R.id.rv_banner, "field 'rvBanner'", RecyclerView.class);
    target.indicatorRecommentdationBeverage = Utils.findRequiredViewAsType(source, R.id.indicator_recommentdation_beverage, "field 'indicatorRecommentdationBeverage'", IndefinitePagerIndicator.class);
    target.tvMoreUpcoming = Utils.findRequiredViewAsType(source, R.id.tv_more_upcoming, "field 'tvMoreUpcoming'", TextView.class);
    target.rvUpcomingEvents = Utils.findRequiredViewAsType(source, R.id.rv_upcoming_events, "field 'rvUpcomingEvents'", RecyclerView.class);
    target.rvRecommendation = Utils.findRequiredViewAsType(source, R.id.rv_recommendation, "field 'rvRecommendation'", RecyclerView.class);
    target.indicatorRecommentdation = Utils.findRequiredViewAsType(source, R.id.indicator_recommentdation, "field 'indicatorRecommentdation'", IndefinitePagerIndicator.class);
    target.rvSports = Utils.findRequiredViewAsType(source, R.id.rv_sports, "field 'rvSports'", RecyclerView.class);
    target.rvMeditation = Utils.findRequiredViewAsType(source, R.id.rv_meditation, "field 'rvMeditation'", RecyclerView.class);
    target.ivMeditation = Utils.findRequiredViewAsType(source, R.id.iv_meditation, "field 'ivMeditation'", ImageView.class);
    target.cvMeditation = Utils.findRequiredViewAsType(source, R.id.cv_meditation, "field 'cvMeditation'", CardView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HomeFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.rvBanner = null;
    target.indicatorRecommentdationBeverage = null;
    target.tvMoreUpcoming = null;
    target.rvUpcomingEvents = null;
    target.rvRecommendation = null;
    target.indicatorRecommentdation = null;
    target.rvSports = null;
    target.rvMeditation = null;
    target.ivMeditation = null;
    target.cvMeditation = null;
  }
}
