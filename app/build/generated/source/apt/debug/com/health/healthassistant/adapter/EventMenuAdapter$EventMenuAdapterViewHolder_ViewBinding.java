// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class EventMenuAdapter$EventMenuAdapterViewHolder_ViewBinding implements Unbinder {
  private EventMenuAdapter.EventMenuAdapterViewHolder target;

  @UiThread
  public EventMenuAdapter$EventMenuAdapterViewHolder_ViewBinding(EventMenuAdapter.EventMenuAdapterViewHolder target,
      View source) {
    this.target = target;

    target.ivFeeds = Utils.findRequiredViewAsType(source, R.id.iv_feeds, "field 'ivFeeds'", ImageView.class);
    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", TextView.class);
    target.tvDate = Utils.findRequiredViewAsType(source, R.id.tv_date, "field 'tvDate'", TextView.class);
    target.tvLocation = Utils.findRequiredViewAsType(source, R.id.tv_location, "field 'tvLocation'", TextView.class);
    target.tvPoints = Utils.findRequiredViewAsType(source, R.id.tv_points, "field 'tvPoints'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    EventMenuAdapter.EventMenuAdapterViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivFeeds = null;
    target.tvTitle = null;
    target.tvDate = null;
    target.tvLocation = null;
    target.tvPoints = null;
  }
}
