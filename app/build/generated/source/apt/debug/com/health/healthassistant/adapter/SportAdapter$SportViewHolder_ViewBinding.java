// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SportAdapter$SportViewHolder_ViewBinding implements Unbinder {
  private SportAdapter.SportViewHolder target;

  @UiThread
  public SportAdapter$SportViewHolder_ViewBinding(SportAdapter.SportViewHolder target,
      View source) {
    this.target = target;

    target.ivIcon = Utils.findRequiredViewAsType(source, R.id.iv_icon, "field 'ivIcon'", ImageView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'tvName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SportAdapter.SportViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivIcon = null;
    target.tvName = null;
  }
}
