// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MeditationMenuAdapter$MeditationMenuViewHolder_ViewBinding implements Unbinder {
  private MeditationMenuAdapter.MeditationMenuViewHolder target;

  @UiThread
  public MeditationMenuAdapter$MeditationMenuViewHolder_ViewBinding(MeditationMenuAdapter.MeditationMenuViewHolder target,
      View source) {
    this.target = target;

    target.tvTitle = Utils.findRequiredViewAsType(source, R.id.tv_title, "field 'tvTitle'", AppCompatTextView.class);
    target.tvName = Utils.findRequiredViewAsType(source, R.id.tv_name, "field 'tvName'", AppCompatTextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MeditationMenuAdapter.MeditationMenuViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tvTitle = null;
    target.tvName = null;
  }
}
