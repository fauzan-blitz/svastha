// Generated code from Butter Knife. Do not modify!
package com.health.healthassistant.view.home;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.health.healthassistant.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class HomeActivity_ViewBinding implements Unbinder {
  private HomeActivity target;

  @UiThread
  public HomeActivity_ViewBinding(HomeActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public HomeActivity_ViewBinding(HomeActivity target, View source) {
    this.target = target;

    target.ivAccount = Utils.findRequiredViewAsType(source, R.id.iv_account, "field 'ivAccount'", ImageView.class);
    target.ivNotif = Utils.findRequiredViewAsType(source, R.id.iv_notif, "field 'ivNotif'", ImageView.class);
    target.toolbar = Utils.findRequiredViewAsType(source, R.id.toolbar, "field 'toolbar'", RelativeLayout.class);
    target.flContainer = Utils.findRequiredViewAsType(source, R.id.fl_container, "field 'flContainer'", FrameLayout.class);
    target.tab = Utils.findRequiredViewAsType(source, R.id.tab, "field 'tab'", TabLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    HomeActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.ivAccount = null;
    target.ivNotif = null;
    target.toolbar = null;
    target.flContainer = null;
    target.tab = null;
  }
}
