package com.health.healthassistant;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class BmiActivity extends AppCompatActivity {

    private EditText edtWeight, edtHeigth;
    private TextView tvScore, tvStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmi);

        Button btnCount;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Count Your Body Mass Index");

        edtHeigth = findViewById(R.id.edt_height1);
        edtWeight = findViewById(R.id.edt_weight1);

        tvScore = findViewById(R.id.tv_ibm_score);
        tvStatus = findViewById(R.id.tv_itm_status);

        btnCount = findViewById(R.id.btn_count);

        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Float height = Float.valueOf(edtHeigth.getText().toString()) ;
                Float weight = Float.valueOf(edtWeight.getText().toString()) ;
                Float score = weight/((height/100)*(height/100));
                String status;
                tvScore.setText(String.valueOf(score));
                if (score<18.5){
                    status = "Underweight";
                } else if (score>18.4&&score<26){
                    status = "Normal";
                } else {
                    status = "Overweight";
                }
                tvStatus.setText(status);
            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
