package com.health.healthassistant;

import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.response.BaseResponse;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BmiFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    private static final String ARG_PARAM2 = "param2";

    @BindView(R.id.tv_ibm_score)
    TextView tvIbmScore;

    @BindView(R.id.edt_height)
    EditText edtHeight;

    @BindView(R.id.edt_weight1)
    EditText edtWeight;

    @BindView(R.id.edt_age)
    EditText edtAge;

    @BindView(R.id.btn_count)
    Button btnCount;

    @BindView(R.id.tv_itm_status)
    TextView tvItmStatus;

    Unbinder unbinder;

    Api apiInterface;

    SharedPreferences sharedPreferences;

    @BindView(R.id.rb_male)
    RadioButton rbMale;

    @BindView(R.id.rb_female)
    RadioButton rbFemale;

    @BindView(R.id.rg_gender)
    RadioGroup rgGender;

    @BindView(R.id.tv_weight)
    TextView tvWeight;

    @BindView(R.id.tv_height)
    TextView tvHeight;

    @BindView(R.id.tv_count_score)
    TextView tvCountScore;

    @BindView(R.id.view_bmi_formula)
    LinearLayout viewBmiFormula;

    public BmiFragment() {
        // Required empty public constructor
    }

    public static BmiFragment newInstance() {
        BmiFragment fragment = new BmiFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String heightString = edtHeight.getText().toString();
                String weightString = edtWeight.getText().toString();
                if (!TextUtils.isEmpty(heightString) && !TextUtils.isEmpty(weightString)) {
                    Float height = Float.valueOf(heightString);
                    Float weight = Float.valueOf(weightString);
                    Float score = weight / ((height / 100) * (height / 100));
                    String status;
                    tvIbmScore.setText(String.valueOf(Math.round(score)));
                    if (score < 18.5) {
                        status = "Underweight";
                    } else if (score > 18.4 && score < 26) {
                        status = "Normal";
                    } else {
                        status = "Overweight";
                    }
                    tvIbmScore.requestFocus();
                    tvItmStatus.setText(status);
                    tvHeight.setText(String.valueOf(height / 100));
                    tvWeight.setText(weightString);
                    tvCountScore.setText(String.valueOf(Math.round(score)));
                    viewBmiFormula.setVisibility(View.VISIBLE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(PrefreceKey.KEY_HEIGHT, heightString);
                    editor.putString(PrefreceKey.KEY_WEIGHT, weightString);
                    editor.putString(PrefreceKey.KEY_SCORE, String.valueOf(Math.round(score)));
                    editor.putString(PrefreceKey.KEY_BMI_STATUS, status);
                    editor.apply();
                    callAPI(score);
                }

            }
        });
    }

    private void callAPI(Float score) {
        Call<BaseResponse> call = apiInterface
            .postBmi(sharedPreferences.getString(PrefreceKey.KEY_ID, ""), String.valueOf(score));
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                Toast.makeText(getContext(), "Your BMI Succesfully Update", Toast.LENGTH_SHORT)
                    .show();
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bmi, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(Api.class);
        sharedPreferences = getContext().getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        initUI();
        return view;
    }

    private void initUI() {
        String height = sharedPreferences.getString(PrefreceKey.KEY_HEIGHT, "");
        String weight = sharedPreferences.getString(PrefreceKey.KEY_WEIGHT, "");
        String score = sharedPreferences.getString(PrefreceKey.KEY_SCORE, "-");
        String status = sharedPreferences.getString(PrefreceKey.KEY_BMI_STATUS, "");

        edtHeight.setText(height);
        edtWeight.setText(weight);
        tvIbmScore.setText(score);
        tvItmStatus.setText(status);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
