package com.health.healthassistant.adapter;

import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.MeditationMenu;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;

public class MeditationMenuAdapter extends BaseRecyclerAdapter<MeditationMenu,
    MeditationMenuAdapter.MeditationMenuViewHolder> {
    public MeditationMenuAdapter(Context context) {
        super(context);
    }

    public MeditationMenuAdapter(Context context, List<MeditationMenu> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_meditation;
    }

    @Override
    public MeditationMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MeditationMenuViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class MeditationMenuViewHolder extends BaseItemViewHolder<MeditationMenu> {

        @BindView(R.id.tv_title)
        AppCompatTextView tvTitle;

        @BindView(R.id.tv_name)
        AppCompatTextView tvName;

        public MeditationMenuViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(MeditationMenu data) {
            tvName.setText(data.getKategori());
            tvTitle.setText(data.getNamameditasi());
        }
    }
}