package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.Events;
import com.health.healthassistant.helper.CommonUtils;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class EventAdapter extends BaseRecyclerAdapter<Events, EventAdapter.EventViewHolder> {

    public EventAdapter(Context context) {
        super(context);
    }

    public EventAdapter(Context context, List<Events> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_upcoming_events;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class EventViewHolder extends BaseItemViewHolder<Events> {

        @BindView(R.id.iv_feeds)
        ImageView ivFeeds;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_category)
        TextView tvCategory;

        public EventViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(Events data) {
            Glide
                .with(mContext)
                .load(data.getPicture())
                .apply(new RequestOptions()
                    .placeholder(R.drawable.food_drinks_default_image)
                    .centerCrop())
                .into(ivFeeds);
            tvTitle.setText(data.getTitle());
            tvCategory.setText(CommonUtils.getReadableHumanDateWithTime(data.getDate()));
        }
    }
}