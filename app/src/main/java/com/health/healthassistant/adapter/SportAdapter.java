package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.Sport;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class SportAdapter extends BaseRecyclerAdapter<Sport, SportAdapter.SportViewHolder> {

    public SportAdapter(Context context) {
        super(context);
    }

    public SportAdapter(Context context, List<Sport> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_sport;
    }

    @Override
    public SportViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SportViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class SportViewHolder extends BaseItemViewHolder<Sport> {

        @BindView(R.id.iv_icon)
        ImageView ivIcon;

        @BindView(R.id.tv_name)
        TextView tvName;

        public SportViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(Sport data) {
            Glide.with(mContext).load(data.getImg()).into(ivIcon);
            tvName.setText(data.getName());
        }
    }
}