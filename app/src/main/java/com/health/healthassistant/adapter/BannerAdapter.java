package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.Banner;
import com.health.healthassistant.view.sport.SportDetailActivity;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import butterknife.BindView;

public class BannerAdapter extends BaseRecyclerAdapter<Banner, BannerAdapter.BannerViewHolder> {

    public BannerAdapter(Context context) {
        super(context);
    }

    public BannerAdapter(Context context, List<Banner> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_banner;
    }

    @Override
    public BannerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BannerViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class BannerViewHolder extends BaseItemViewHolder<Banner> {

        @BindView(R.id.iv_banner)
        ImageView ivBanner;

        public BannerViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(Banner data) {
            Glide
                .with(mContext)
                .load(data.getImg())
                .apply(new RequestOptions()
                    .placeholder(R.drawable.food_drinks_default_image)
                    .centerCrop())
                .into(ivBanner);
        }
    }
}