package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.Events;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class EventMenuAdapter extends BaseRecyclerAdapter<Events, EventMenuAdapter
    .EventMenuAdapterViewHolder> {

    public EventMenuAdapter(Context context) {
        super(context);
    }

    public EventMenuAdapter(Context context, List<Events> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_event;
    }

    @Override
    public EventMenuAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventMenuAdapterViewHolder(mContext, getView(parent, viewType),
            mItemClickListener, mLongItemClickListener);
    }

    public class EventMenuAdapterViewHolder extends BaseItemViewHolder<Events> {

        @BindView(R.id.iv_feeds)
        ImageView ivFeeds;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_date)
        TextView tvDate;

        @BindView(R.id.tv_location)
        TextView tvLocation;

        @BindView(R.id.tv_points)
        TextView tvPoints;

        public EventMenuAdapterViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(Events data) {
            Glide
                .with(mContext)
                .load(data.getPicture())
                .apply(new RequestOptions()
                    .placeholder(R.drawable.food_drinks_default_image)
                    .centerCrop())
                .into(ivFeeds);
            tvTitle.setText(data.getTitle());
            tvDate.setText(data.getDate());
            tvLocation.setText(data.getPlace());
            tvPoints.setText(String.format("%s pts",data.getRewardpoints()));
        }
    }
}