package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.SportList;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class SportListAdapter extends BaseRecyclerAdapter<SportList, SportListAdapter
    .SportListViewHolder> {

    public SportListAdapter(Context context) {
        super(context);
    }

    public SportListAdapter(Context context, List<SportList> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_sport_list;
    }

    @Override
    public SportListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SportListViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class SportListViewHolder extends BaseItemViewHolder<SportList> {

        @BindView(R.id.iv_feeds)
        ImageView ivFeeds;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_date)
        TextView tvDate;

        public SportListViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(SportList data) {
            Glide
                .with(mContext)
                .load(data.getImage())
                .apply(new RequestOptions()
                .placeholder(R.drawable.food_drinks_default_image)
                .centerCrop())
                .into(ivFeeds);
            tvTitle.setText(data.getTitle());
            tvDate.setText(data.getId());
        }
    }
}