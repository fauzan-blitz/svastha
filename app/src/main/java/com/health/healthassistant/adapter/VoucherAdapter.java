package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.Voucher;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class VoucherAdapter extends BaseRecyclerAdapter<Voucher, VoucherAdapter.VoucherViewHolder> {

    public VoucherAdapter(Context context) {
        super(context);
    }

    public VoucherAdapter(Context context, List<Voucher> data) {
        super(context, data);
    }

    OnRedeemButtonClicked onRedeemButtonClicked;

    public OnRedeemButtonClicked getOnRedeemButtonClicked() {
        return onRedeemButtonClicked;
    }

    public void setOnRedeemButtonClicked(
        OnRedeemButtonClicked onRedeemButtonClicked) {
        this.onRedeemButtonClicked = onRedeemButtonClicked;
    }

    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_voucher;
    }

    @Override
    public VoucherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new VoucherViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class VoucherViewHolder extends BaseItemViewHolder<Voucher> {

        @BindView(R.id.iv_feeds)
        ImageView ivFeeds;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_points)
        TextView tvPoints;

        @BindView(R.id.tv_redeemed)
        TextView tvRedeemed;

        @BindView(R.id.btn_redeem)
        Button btnRedeem;

        public VoucherViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(final Voucher data) {
            Glide
                .with(mContext)
                .load(data.getImage())
                .apply(new RequestOptions()
                    .placeholder(R.drawable.food_drinks_default_image)
                    .centerCrop())
                .into(ivFeeds);

            tvTitle.setText(data.getName());
            tvPoints.setText(String.format("%s points",data.getPoint()));
            tvRedeemed.setVisibility(data.getIsRedeem() == 0? View.GONE:View.VISIBLE);
            btnRedeem.setVisibility(data.getIsRedeem() == 1? View.GONE:View.VISIBLE);
            btnRedeem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onRedeemButtonClicked.redeemButtonListener(data.getId(),getAdapterPosition(), data.getName());
                }
            });
        }
    }

    public interface OnRedeemButtonClicked{
        void redeemButtonListener(String voucherCode, int position, String voucherName);
    }
}