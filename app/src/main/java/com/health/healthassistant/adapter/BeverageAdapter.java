package com.health.healthassistant.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BaseItemViewHolder;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.data.model.entity.Beverage;
import com.health.healthassistant.data.model.entity.Food;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;

public class BeverageAdapter extends BaseRecyclerAdapter<Food, BeverageAdapter
    .BeverageViewHolder> {

    public BeverageAdapter(Context context) {
        super(context);
    }

    public BeverageAdapter(Context context, List<Food> data) {
        super(context, data);
    }


    @Override
    protected int getItemResourceLayout(int viewType) {
        return R.layout.item_feed;
    }

    @Override
    public BeverageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BeverageViewHolder(mContext, getView(parent, viewType), mItemClickListener,
            mLongItemClickListener);
    }

    public class BeverageViewHolder extends BaseItemViewHolder<Food> {

        @BindView(R.id.iv_feeds)
        ImageView ivFeeds;

        @BindView(R.id.tv_title)
        TextView tvTitle;

        @BindView(R.id.tv_category)
        TextView tvCategory;

        public BeverageViewHolder(Context mContext, View itemView,
            OnItemClickListener itemClickListener, OnLongItemClickListener longItemClickListener) {
            super(mContext, itemView, itemClickListener, longItemClickListener);
        }

        @Override
        public void bind(Food data) {
            tvTitle.setText(data.getName());
            tvCategory.setText(data.getDiettype());
            Glide
                .with(mContext)
                .load(data.getImage())
                .apply(new RequestOptions()
                    .placeholder(R.drawable.food_drinks_default_image)
                    .centerCrop())
                .into(ivFeeds);
        }
    }
}