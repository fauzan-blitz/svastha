package com.health.healthassistant.helper;

import com.health.healthassistant.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MessageFactory {

    public static ProgressDialog showLoadingDialog(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.show();
//        if (progressDialog.getWindow() != null) {
//            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color
// .TRANSPARENT));
//        }

        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public static void showToast(Context context,String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }

    public static void showLongToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void showSnackbarMessage(Context context, View view, String message) {

        Snackbar snackbar = Snackbar.make(view,
            message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
            .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        snackbar.show();
    }

    public static void showSnackbarMessage(Context context, View view, String message, String actionName,
        View.OnClickListener actionListener) {
        Snackbar snackbar = Snackbar.make(view,
            message, Snackbar.LENGTH_SHORT);
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView
            .findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
        snackbar.setAction(actionName, actionListener);

        snackbar.show();
    }

    public static void showAlertDialog(Context context, String message) {
        new AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
    }


    public static void showAlertDialog(Context context, String message
        , String positive
        , DialogInterface.OnClickListener positiveListener) {

        new AlertDialog.Builder(context)
            .setMessage(message)
            .setPositiveButton(positive, positiveListener).show();
    }

    public static void showAlertDialog(Context context, String title, String message) {
        new AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
    }

    public static void showAlertDialog(Context context, String title
        , String message
        , String positive
        , DialogInterface.OnClickListener positiveListener) {

        new AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positive, positiveListener).show();
    }

    public static void showAlertDialog(Context context, String title
        , String message
        , String positive
        , DialogInterface.OnClickListener positiveListener
        , String negative
        , DialogInterface.OnClickListener negativeListener) {

        new AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(positive, positiveListener)
            .setNegativeButton(negative, negativeListener)
            .show();
    }


    public interface MyDialogActionListener {

        void action();
        void action(View v);

    }

    public interface MyDialogWithCheckBoxActionListener {

        void action(boolean checkBoxChecked);
    }

    public static void showVoucherCodeAlert(@NonNull final Activity context,
        @NonNull String label,
        @NonNull String name,
        @NonNull String category,
        @NonNull String voucherCode,
        @NonNull String info,
        @Nullable final MyDialogActionListener cancelListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        final View view = context.getLayoutInflater().inflate(R.layout.alert_voucher_code, null);
        ImageView ivAlertIcon = view.findViewById(R.id.iv_alert_icon);
        TextView tvLabel = view.findViewById(R.id.tv_label);
        TextView tvName = view.findViewById(R.id.tv_name);
        TextView tvCategory = view.findViewById(R.id.tv_category);
        TextView tvAlertCode = view.findViewById(R.id.tv_voucher_code);
        TextView tvInfo = view.findViewById(R.id.tv_info);
        Button btnOk = view.findViewById(R.id.btn_alert_positive);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.show();

        tvLabel.setText(label);
        tvName.setText(name);
        tvAlertCode.setText(voucherCode);
        tvCategory.setText(category);
        tvInfo.setText(info);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (cancelListener != null) {
                    cancelListener.action(view);
                }
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                if (cancelListener != null) {
                    cancelListener.action(view);
                }
            }
        });
    }

}
