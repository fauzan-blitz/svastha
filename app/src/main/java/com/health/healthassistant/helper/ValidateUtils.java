package com.health.healthassistant.helper;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public class ValidateUtils {

    public static boolean validate(View... views) {
        return validate(null, views);
    }

    public static boolean validate(String errorMessage, View... views) {
        boolean isValid = true;
        for (View view : views) {
            if (view instanceof EditText) {
                if (!isValid)
                    isNotEmpty((EditText) view, errorMessage);
                else
                    isValid = isNotEmpty((EditText) view, errorMessage);
            } else if (view instanceof TextInputLayout){
                if (!isValid)
                    isNotEmpty((TextInputLayout) view, errorMessage);
                else
                    isValid = isNotEmpty((TextInputLayout) view, errorMessage);
            }
        }
        return isValid;
    }


    public static boolean isNotEmpty(EditText editText) {
        return isNotEmpty(editText, null);
    }

    public static boolean isNotEmpty(EditText editText, String errorMessage) {
        String text = editText.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            if (errorMessage != null)
                editText.setError(errorMessage);
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public static boolean isNotEmpty(TextInputLayout editText, String errorMessage) {
        String text = editText.getEditText().getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            if (errorMessage != null)
                editText.setError(errorMessage);
            editText.requestFocus();
            return false;
        }
        return true;
    }


    //Validate edittext with texinputlayout
    public static boolean validate(String errorMessage, EditText[] editTexts, TextInputLayout[] textInputLayouts) {
        boolean isValid = true;
        int tilCounter = 0;
        for (EditText editText : editTexts) {
            if (!isValid){
                isNotEmpty(editText,textInputLayouts[tilCounter], errorMessage);
            }
            else{
                isValid = isNotEmpty(editText,textInputLayouts[tilCounter], errorMessage);
            }
            tilCounter++;

        }
        return isValid;
    }

    public static boolean isNotEmpty(EditText editText,TextInputLayout textInputLayout, String errorMessage) {
        String text = editText.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            if (errorMessage != null)
                textInputLayout.setError(errorMessage);
            editText.requestFocus();
            return false;
        }
        return true;
    }

    public static void editTextChange(EditText editText, final TextInputLayout textInputLayout, final String errorMessage){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    textInputLayout.setError(errorMessage);
                } else {
                    textInputLayout.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void editTextChange(EditText[] editTexts, final TextInputLayout[] textInputLayouts, final String errorMessage){
        int tilCounter = 0;
        for (EditText editText: editTexts){
            final int finalTilCounter = tilCounter;
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() == 0){
                        textInputLayouts[finalTilCounter].setError(errorMessage);
                    } else {
                        textInputLayouts[finalTilCounter].setError(null);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            tilCounter++;
        }

    }

    public static void emailEditTextChecker(EditText editText, final TextInputLayout textInputLayout){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    textInputLayout.setError("Field Required");
                } else {
                    if (!CommonUtils.isEmailValid(s.toString())){
                        textInputLayout.setError("Email not Valid");
                    } else {
                        textInputLayout.setError(null);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void passwordEditTextChecker(EditText editText, final TextInputLayout textInputLayout){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    textInputLayout.setError("Field Required");
                } else {
                    if (!CommonUtils.isPasswordStrongEnough(s.toString())){
                        textInputLayout.setError("Password Not Strong");
                    } else {
                        textInputLayout.setError(null);
                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void phoneNumberEditTextChecker(EditText editText, final TextInputLayout textInputLayout){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0){
                    textInputLayout.setError("Field Required");
                } else {
                    if (s.length() <12){
                        if (!CommonUtils.isPhoneNumber(s.toString())){
                            textInputLayout.setError("Phone Number Invalid");
                        } else {
                            textInputLayout.setError(null);
                        }
                    }

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public static void dateEditTextChecker(final EditText editText){
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length()<10){
                    editText.setError("Date Format not complete");
                } else {
                    editText.setError(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                StringBuilder string = new StringBuilder(s);
                if (s.length() == 4 || s.length() == 7) {
                    string.append('-');
                    editText.setText(string);
                    editText.setSelection(s.length()+1);
                }
            }
        });
    }

}
