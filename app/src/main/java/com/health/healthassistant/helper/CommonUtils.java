package com.health.healthassistant.helper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.provider.Settings;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonUtils {
    private static final String TAG = CommonUtils.class.getSimpleName();

    private ProgressDialog progressDialog;

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean isEmailValid(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean isPhoneNumber(String phone) {
        Pattern pattern;
        Matcher matcher;
        final String PHONE_PATTERN = "^(?=.*[0-9])$";
        pattern = Pattern.compile(PHONE_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public static boolean isPasswordStrongEnough(final String password) {
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^[a-zA-Z\\d@#$%^&+=!~]{8,}$";
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static boolean idNumber(String phone) {
        Pattern pattern;
        Matcher matcher;
        final String ID_NUMBER_PATTERN = "^(?=.*[0-9])$";
        pattern = Pattern.compile(ID_NUMBER_PATTERN);
        matcher = pattern.matcher(phone);
        return matcher.matches();
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName)
        throws IOException {

        AssetManager manager = context.getAssets();
        InputStream is = manager.open(jsonFileName);

        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        return new String(buffer, "UTF-8");
    }

    public static boolean isMyServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMyAppRunning(Context context, String packageName) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        boolean run = false;
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                if (appProcess.processName.equals(packageName)) {
                    run = true;
                    break;
                }
            }
        }

        return run;
    }

    public static Map<String, String> mapObject(Object o) {
        Gson gson = new Gson();

        String json = gson.toJson(o, o.getClass());
        Map<String, String> map = new HashMap<>();

        Type type = new TypeToken<Map<String, String>>() {
        }.getType();

        map = gson.fromJson(json, type);
        return map;
    }


    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static SpannableString getColoredString(String text, int color) {
        SpannableString word = new SpannableString(text);
        word.setSpan(new ForegroundColorSpan(color), 0, word.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return word;
    }



    public static String getReadableHumanDate(String date) {
        String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei",
            "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
        //2017-08-13
        String[] d = date.split("T");
        String m[] = d[0].split("-");
        String readableDate = m[2] + " " + months[Integer.parseInt(m[1])-1] + " " + m[0];

        return readableDate;
    }

    public static String getReadableHumanDateWithTime(String date) {
        String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "Mei",
            "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"};
        //2017-08-13
        String[] d = date.split(" ");
        String m[] = d[0].split("-");
        String h[] = d[1].split(":");
        String readableDate = m[2] + " " + months[Integer.parseInt(m[1])-1] + " " + m[0];
        String readableTime = h[0]+":"+h[1];

        return readableDate+"    "+readableTime;
    }

    public static String getMiniTime(String hour) {
        String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "Mei",
            "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"};
        //2017-08-13
        String h[] = hour.split(":");
        String readableTime = h[0]+":"+h[1];

        return readableTime;
    }

    public static String getMiniReadableHumanDate(String date) {
        String[] months = new String[]{"Jan", "Feb", "Mar", "Apr", "Mei",
            "Jun", "Jul", "Agu", "Sep", "Okt", "Nov", "Des"};
        //2017-08-13
        String m[] = date.split("-");
        String readableDate = m[2] + " " + months[Integer.parseInt(m[1])-1] + " " + m[0];

        return readableDate;
    }

    public static String getDate(String date) {
        //2017-08-13
        String[] d = date.split("T");
        String m[] = d[0].split("-");
        String readableDate = d[0];

        return readableDate;
    }

    public static boolean isConfirmPasswordValid(String password, String confirmPassword) {
        if (password.equals(confirmPassword)) {
            return true;
        } else {
            return false;
        }
    }

    public static String reDate(String date) {
        String[] months = new String[]{"Januari", "Februari", "Maret", "April", "Mei",
            "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
        ArrayList<String> month = new ArrayList<>(Arrays.asList(months));
        String[] d = date.split(" ");
        String readableDate = d[2] + "-" +month.indexOf(d[1]) + "-" + d[0];

        return readableDate;
    }

    public static void intentMessage(String phoneNumber , Context context){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("smsto:" + Uri.encode(phoneNumber)));
        context.startActivity(intent);
    }

    public static void sendEmail(Context context, String subject, String message) {

        String[] TO = {"cs@amanbos.com"};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");

        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, message);

        context.startActivity(Intent.createChooser(emailIntent, "Select Email Sending App : "));
    }

    public static String getService(String name) {
        return name.equalsIgnoreCase("escort")? "Aman Kawal" : "Aman Jaga";
    }

    public static void showLoadingDialog(Context context, boolean show) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading. Please Wait");
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color
 .TRANSPARENT));
        }

        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        if (show){
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
            progressDialog.show();
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        }
    }
}
