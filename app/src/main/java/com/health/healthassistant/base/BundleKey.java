package com.health.healthassistant.base;

public class BundleKey {

    public static final String KEY_CATEGORY_ID = "KEY_CATEGORY_ID";
    public static final String KEY_CATEGORY_NAME = "KEY_CATEGORY_NAME";
    public static final String KEY_CATEGORY_IMG = "KEY_CATEGORY_IMG";
    public static final String KEY_CATEGORY_DESC = "KEY_CATEGORY_DESC";
    public static final String KEY_CATEGORY_TITLE = "KEY_CATEGORY_TITLE";

    public static final String KEY_IS_EDIT_REGISTER = "KEY_IS_EDIT_REGISTER";
}
