package com.health.healthassistant.base;

public class PrefreceKey {
    public static final String mypreference = "mypref";
    public static final String KEY_NAME = "user_name";
    public static final String KEY_LAST_NAME = "user_last_name";
    public static final String KEY_ID = "user_id";
    public static final String KEY_EMAIL = "user_email";
    public static final String KEY_BIRTHDATE = "user_birthdate";
    public static final String KEY_PHONE = "user_phone";
    public static final String KEY_GENDER = "user_gender";
    public static final String KEY_POINT = "user_point";
    public static final String KEY_HEIGHT = "bmi_height";
    public static final String KEY_WEIGHT = "bmi_WEIGHT";
    public static final String KEY_SCORE = "bmi_score";
    public static final String KEY_BMI_STATUS = "bmi_status";

}
