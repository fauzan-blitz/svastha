package com.health.healthassistant.base;

import com.androidnetworking.AndroidNetworking;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState,
        @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        setContentView(getLayoutResource());
        ButterKnife.bind(this);
        AndroidNetworking.initialize(getApplicationContext());

        // endregion

        onViewReady();
    }

    private void onViewReady() {
        initUI();
        initAction();
    }

    //    pass Layout here
    protected abstract int getLayoutResource();

    //    initialize the UI, setup toolbar, setText etc here
    protected abstract void initUI();

    //    initialize UI interaction here
    protected abstract void initAction();
}
