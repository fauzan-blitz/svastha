package com.health.healthassistant;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExerciseDetailActivity extends AppCompatActivity {
    ProgressDialog progressDialog;
    ListView lv;
    ImageView iv;
    int categoryId;
    JSONParser jsonParser = new JSONParser();
    String url = "http://cardisc.co/api_health/getExerciseByCategory.php";
    ArrayList<HashMap<String, String>> list_instruksi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_detail);
        categoryId = getIntent().getIntExtra(IntentKey.EXERCISE,0);

        Toolbar toolbar = findViewById(R.id.toolbar);
        lv = findViewById(R.id.lv_step);
        iv = findViewById(R.id.iv);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (categoryId == 1){
            getSupportActionBar().setTitle("Run");
            iv.setImageResource(R.drawable.menu1);
        }

        switch (categoryId){
            case 1 :
                getSupportActionBar().setTitle("Run");
                iv.setImageResource(R.drawable.menu1);
                break;
            case 2:
                getSupportActionBar().setTitle("Jump");
                iv.setImageResource(R.drawable.menu2);
                break;
            case 3:
                getSupportActionBar().setTitle("Throw");
                iv.setImageResource(R.drawable.menu3);
                break;
        }



        loadData(categoryId);

    }

    private void loadData(final int categoryId) {

        class LoadExercise extends AsyncTask<String, String, String> {
            @Override
            protected void onPreExecute() {

                super.onPreExecute();

                progressDialog = new ProgressDialog(ExerciseDetailActivity.this);
                progressDialog.setMessage("Proses Mengambil Data");
                progressDialog.setCancelable(true);
                progressDialog.show();
            }

            @Override
            protected String doInBackground(String... strings) {
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("category",String.valueOf(categoryId)));
                JSONObject jsonObject = jsonParser.makeHttpRequest(url,"GET",param);
                try {
                    JSONArray data = jsonObject.getJSONArray("data");
                    for (int i=0; i<data.length(); i++ ) {
                        JSONObject f = data.getJSONObject(i);
                        String id = f.getString("id");
                        String title = f.getString("title");
                        String img = f.getString("img");
                        String description = f.getString("description");
                        String category = f.getString("category");

                        HashMap<String, String> map = new HashMap<>();
                        map.put("id", id);
                        map.put("title", title);
                        map.put("description", description);
                        map.put("img", img);
                        map.put("category", category);
                        if (list_instruksi == null){
                            list_instruksi = new ArrayList<>();
                        }
                        list_instruksi.add(map);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                ListAdapter listAdapter = new SimpleAdapter(getApplicationContext(),list_instruksi,R.layout.item_instruksi,new String[]{"title","description"},new int[]{
                        R.id.tv_title,R.id.tv_descirption
                });
                lv.setAdapter(listAdapter);
                progressDialog.dismiss();
            }
        }
        LoadExercise loadExercise = new LoadExercise();
        loadExercise.execute();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
//            Intent intent = new Intent(getApplicationContext(),MainActivity.class);
//            startActivity(intent);
//            finish();
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

}
