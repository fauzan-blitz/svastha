package com.health.healthassistant.data.model.response;

import com.google.gson.annotations.SerializedName;

import com.health.healthassistant.data.model.entity.Voucher;

import java.util.ArrayList;

public class VoucherResponse extends BaseResponse{
    @SerializedName("data")
    private ArrayList<Voucher> vouchers;

    public ArrayList<Voucher> getVouchers() {
        return vouchers;
    }

    public void setVouchers(
        ArrayList<Voucher> vouchers) {
        this.vouchers = vouchers;
    }
}
