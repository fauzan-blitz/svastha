package com.health.healthassistant.data.model.entity;

import com.google.gson.annotations.SerializedName;

public class SportList {

    @SerializedName("description")
    private String description;

    @SerializedName("title")
    private String title;

    @SerializedName("id")
    private String id;

    @SerializedName("image")
    private String image;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
