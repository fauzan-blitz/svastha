package com.health.healthassistant.data.model.entity;

public class Feed {

    private int image;
    private String title;
    private String category;
    private int love;

    public Feed(int image, String title, String category, int love) {
        this.image = image;
        this.title = title;
        this.category = category;
        this.love = love;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getLove() {
        return love;
    }

    public void setLove(int love) {
        this.love = love;
    }
}
