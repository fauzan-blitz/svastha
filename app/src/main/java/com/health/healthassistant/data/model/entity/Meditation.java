package com.health.healthassistant.data.model.entity;

import com.google.gson.annotations.SerializedName;

public class Meditation {


    @SerializedName("kategori")
    private String kategori;

    @SerializedName("gambar")
    private String gambar;

    @SerializedName("deskripsi")
    private String deskripsi;

    @SerializedName("namaMeditasi")
    private String namameditasi;

    @SerializedName("id")
    private String id;

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getNamameditasi() {
        return namameditasi;
    }

    public void setNamameditasi(String namameditasi) {
        this.namameditasi = namameditasi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
