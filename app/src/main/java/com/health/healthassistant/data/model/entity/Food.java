package com.health.healthassistant.data.model.entity;

import com.google.gson.annotations.SerializedName;

public class Food {

    @SerializedName("image")
    private String image;

    @SerializedName("updatedAt")
    private String updatedat;

    @SerializedName("createdAt")
    private String createdat;

    @SerializedName("dietType")
    private String diettype;

    @SerializedName("description")
    private String description;

    @SerializedName("foodCategoryId")
    private String foodcategoryid;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getDiettype() {
        return diettype;
    }

    public void setDiettype(String diettype) {
        this.diettype = diettype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFoodcategoryid() {
        return foodcategoryid;
    }

    public void setFoodcategoryid(String foodcategoryid) {
        this.foodcategoryid = foodcategoryid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
