package com.health.healthassistant.data.api;

import com.health.healthassistant.data.model.request.LoginRequest;
import com.health.healthassistant.data.model.request.UserRequest;
import com.health.healthassistant.data.model.response.BannerResponse;
import com.health.healthassistant.data.model.response.BaseResponse;
import com.health.healthassistant.data.model.response.EventDetailResponse;
import com.health.healthassistant.data.model.response.EventResponse;
import com.health.healthassistant.data.model.response.FoodResponse;
import com.health.healthassistant.data.model.response.JoinEventResponse;
import com.health.healthassistant.data.model.response.LoginResponse;
import com.health.healthassistant.data.model.response.MeditationResponse;
import com.health.healthassistant.data.model.response.ProfileResponse;
import com.health.healthassistant.data.model.response.RedeemResponse;
import com.health.healthassistant.data.model.response.SportResponse;
import com.health.healthassistant.data.model.response.VoucherResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface Api {

    @FormUrlEncoded
    @POST("/index.php/SvasthaApi/register")
    Call<BaseResponse> postRegister(@Field("firstName") String firstName,
        @Field("lastName") String lastName,
        @Field("email") String email,
        @Field("password") String password,
        @Field("phone") String phone,
        @Field("gender") String gender,
        @Field("birthdate") String birthdate);

    @FormUrlEncoded
    @POST("/index.php/SvasthaApi/login")
    Call<LoginResponse> postLogin(@Field("email") String email, @Field("password") String password);

    @GET("/index.php/SvasthaApi/profile/{id}")
    Call<ProfileResponse> getProfile(@Path("id") String id);

    @GET("/index.php/SvasthaApi/listExcerciseByCategory/{id}")
    Call<SportResponse> getSportByCategory(@Path("id") int id);

    @FormUrlEncoded
    @POST("/index.php/SvasthaApi/insertBmi")
    Call<BaseResponse> postBmi(@Field("userId") String userId, @Field("bmi") String bmi);

    @GET("/index.php/SvasthaApi/upcomingEvents")
    Call<EventResponse> getUpcomingEvents();

    @GET("/index.php/SvasthaApi/rekomendasiMakananSehat/{id}")
    Call<FoodResponse> getRecommendationFood(@Path("id") String id);

    @GET("/index.php/SvasthaApi/rekomendasiMinumanSehat/{id}")
    Call<FoodResponse> getRecommendationBeverage(@Path("id") String id);

    @GET("/index.php/SvasthaApi/meditasi")
    Call<MeditationResponse> getMeditation();

    @GET("/index.php/SvasthaApi/event")
    Call<EventResponse> getEvent();

    @GET("/index.php/SvasthaApi/eventDetail/{id}")
    Call<EventDetailResponse> getEventDetail(@Path("id") String id);

    @GET("/index.php/SvasthaApi/getBanner")
    Call<BannerResponse> getBanner();

    @FormUrlEncoded
    @POST("/index.php/SvasthaApi/joinEvent")
    Call<JoinEventResponse> postJoinEvent(@Field("userId") String userId, @Field("eventId") String eventId);

    @FormUrlEncoded
    @POST("/index.php/SvasthaApi/redeemVoucher")
    Call<RedeemResponse> postRedeemVoucher(@Field("userId") String userId, @Field("voucherId") String voucherId);

    @GET("/index.php/SvasthaApi/listVoucher/{id}")
    Call<VoucherResponse> getVoucher(@Path("id") String id);

    @FormUrlEncoded
    @POST("/index.php/SvasthaApi/updateProfile")
    Call<BaseResponse> postEditProfile(
        @Field("userId") String userId,
        @Field("firstName") String firstName,
        @Field("lastName") String lastName,
        @Field("email") String email,
        @Field("phone") String phone,
        @Field("gender") String gender,
        @Field("birthdate") String birthdate);
}
