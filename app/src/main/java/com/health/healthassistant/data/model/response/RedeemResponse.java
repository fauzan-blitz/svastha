package com.health.healthassistant.data.model.response;

public class RedeemResponse extends BaseResponse{

    private String voucherCode;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
}
