package com.health.healthassistant.data.model.response;

import com.google.gson.annotations.SerializedName;

import com.health.healthassistant.data.model.entity.UserEntity;

import java.util.ArrayList;

public class LoginResponse extends BaseResponse {

    @SerializedName("data")
    private ArrayList<UserEntity> data;

    public ArrayList<UserEntity> getData() {
        return data;
    }

    public void setData(ArrayList<UserEntity> data) {
        this.data = data;
    }
}
