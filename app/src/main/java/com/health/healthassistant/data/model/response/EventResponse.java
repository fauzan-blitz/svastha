package com.health.healthassistant.data.model.response;

import com.google.gson.annotations.SerializedName;

import com.health.healthassistant.data.model.entity.Events;
import com.health.healthassistant.data.model.entity.UpcomingEvent;

import java.util.ArrayList;

public class EventResponse extends BaseResponse{
    @SerializedName("data")
    private ArrayList<Events> upcomingEvents;

    public ArrayList<Events> getUpcomingEvents() {
        return upcomingEvents;
    }

    public void setUpcomingEvents(
        ArrayList<Events> upcomingEvents) {
        this.upcomingEvents = upcomingEvents;
    }
}
