package com.health.healthassistant.data.model.response;

import com.google.gson.annotations.SerializedName;

import com.health.healthassistant.data.model.entity.Meditation;
import com.health.healthassistant.data.model.entity.MeditationMenu;

import java.util.ArrayList;

public class MeditationResponse extends BaseResponse{

    @SerializedName("data")
    private ArrayList<MeditationMenu> meditations;

    public ArrayList<MeditationMenu> getMeditations() {
        return meditations;
    }

    public void setMeditations(
        ArrayList<MeditationMenu> meditations) {
        this.meditations = meditations;
    }
}
