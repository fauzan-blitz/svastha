package com.health.healthassistant.data.model.entity;

import com.google.gson.annotations.SerializedName;

public class Events {

    @SerializedName("eventCode")
    private String eventcode;

    @SerializedName("picture")
    private String picture;

    @SerializedName("updatedAt")
    private String updatedat;

    @SerializedName("createdAt")
    private String createdat;

    @SerializedName("rewardPoints")
    private String rewardpoints;

    @SerializedName("description")
    private String description;

    @SerializedName("place")
    private String place;

    @SerializedName("date")
    private String date;

    @SerializedName("title")
    private String title;

    @SerializedName("id")
    private String id;

    public Events(String eventcode, String picture, String updatedat, String createdat,
        String rewardpoints, String description, String place, String date, String title,
        String id) {
        this.eventcode = eventcode;
        this.picture = picture;
        this.updatedat = updatedat;
        this.createdat = createdat;
        this.rewardpoints = rewardpoints;
        this.description = description;
        this.place = place;
        this.date = date;
        this.title = title;
        this.id = id;
    }

    public String getEventcode() {
        return eventcode;
    }

    public void setEventcode(String eventcode) {
        this.eventcode = eventcode;
    }

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getRewardpoints() {
        return rewardpoints;
    }

    public void setRewardpoints(String rewardpoints) {
        this.rewardpoints = rewardpoints;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }
}
