package com.health.healthassistant.data.model.entity;

import com.google.gson.annotations.SerializedName;

public class Voucher {


    @SerializedName("updatedAt")
    private String updatedat;

    @SerializedName("createdAt")
    private String createdat;

    @SerializedName("code")
    private String code;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("point")
    private String point;

    @SerializedName("merchant")
    private String merchant;

    @SerializedName("quota")
    private String quota;

    @SerializedName("expire")
    private String expire;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private String id;

    @SerializedName("isRedeem")
    private int isRedeem;

    public String getUpdatedat() {
        return updatedat;
    }

    public void setUpdatedat(String updatedat) {
        this.updatedat = updatedat;
    }

    public String getCreatedat() {
        return createdat;
    }

    public void setCreatedat(String createdat) {
        this.createdat = createdat;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getMerchant() {
        return merchant;
    }

    public void setMerchant(String merchant) {
        this.merchant = merchant;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getExpire() {
        return expire;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getIsRedeem() {
        return isRedeem;
    }

    public void setIsRedeem(int isRedeem) {
        this.isRedeem = isRedeem;
    }
}
