package com.health.healthassistant.data.model.response;

import com.google.gson.annotations.SerializedName;

import com.health.healthassistant.data.model.entity.SportList;

import java.util.ArrayList;

public class SportResponse extends BaseResponse {
    @SerializedName("data")
    private ArrayList<SportList> sportLists;

    public ArrayList<SportList> getSportLists() {
        return sportLists;
    }

    public void setSportLists(
        ArrayList<SportList> sportLists) {
        this.sportLists = sportLists;
    }
}
