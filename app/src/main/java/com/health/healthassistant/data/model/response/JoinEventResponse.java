package com.health.healthassistant.data.model.response;

public class JoinEventResponse extends BaseResponse {

    private String registrationId;

    public String getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(String registrationId) {
        this.registrationId = registrationId;
    }
}
