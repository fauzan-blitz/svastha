package com.health.healthassistant.data.model.response;

import com.google.gson.annotations.SerializedName;

import com.health.healthassistant.data.model.entity.Food;

import java.util.ArrayList;

public class FoodResponse extends BaseResponse{
    @SerializedName("data")
    private ArrayList<Food> foods;

    public ArrayList<Food> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<Food> foods) {
        this.foods = foods;
    }
}
