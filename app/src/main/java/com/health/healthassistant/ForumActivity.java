package com.health.healthassistant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ForumActivity extends AppCompatActivity {

    String url_tambah_forum = "http://cardisc.co/api_health/getForum.php";
    ProgressDialog progressDialog;
    JSONParser jsonParser = new JSONParser();
    ArrayList<HashMap<String,String>> list_forum;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Kuy Olahraga");

        Button btnTambah = findViewById(R.id.btn_add_forum);
        listView = findViewById(R.id.lv_forum);


        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ForumActivity.this, TambahForumActivity.class);
                startActivity(intent);
                finish();
            }
        });





        loadForum();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadForum() {

        class LoadForum extends AsyncTask<String,String,String> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(ForumActivity.this);
                progressDialog.setMessage("Proses Menambah Posting");
                progressDialog.setCancelable(true);
                progressDialog.show();
            }

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> param = new ArrayList<>();

                JSONObject jsonObject = jsonParser.makeHttpRequest(url_tambah_forum,"GET",param);

                try {
                    JSONArray forum = jsonObject.getJSONArray("data");
                    for (int i=0; i<forum.length(); i++ ){
                        JSONObject f = forum.getJSONObject(i);
                        String id = f.getString("id");
                        String name = f.getString("name");
                        String description = f.getString("description");
                        String date = f.getString("date");
                        String createdAt = f.getString("createdAt");

                        HashMap<String,String> map = new HashMap<>();
                        map.put("id", id);
                        map.put("name",name);
                        map.put("description",description);
                        map.put("date",date);
                        map.put("createdAt",createdAt);
                        if (list_forum == null){
                            list_forum = new ArrayList<>();
                        }
                        list_forum.add(map);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                ListAdapter listAdapter = new SimpleAdapter(ForumActivity.this,list_forum,R.layout.item_forum,new String[]{"name","createdAt","description","date"},new int[]{
                        R.id.tv_name,R.id.tv_createAt,R.id.tv_desc,R.id.tv_date
                });
                listView.setAdapter(listAdapter);
                progressDialog.dismiss();
//                        finish();
            }
        }
        LoadForum loadForum = new LoadForum();
        loadForum.execute();
    }

}
