package com.health.healthassistant;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TambahForumActivity extends AppCompatActivity {

    Button btnBuatPost;
    EditText edtName, edtDesc, edtDate;
    JSONParser jsonParser = new JSONParser();
    String name, desc, date;
    String url_tambah_forum = "http://cardisc.co/api_health/addForum.php";
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_forum);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        btnBuatPost = findViewById(R.id.btn_add_forum);
        edtName = findViewById(R.id.edt_name);
        edtDesc = findViewById(R.id.edt_description);
        edtDate = findViewById(R.id.edt_date);

//        dummyData();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Buat Postingan");
        btnBuatPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtName.length()!=0){
                    if (edtDesc.length()!=0){
                        if (edtDate.length()!=0){
                            tambahForum();

                        }
                    }
                }
            }
        });

    }

    private void dummyData() {
        edtName.setText("maman");
        edtDesc.setText("main futsal di lapangan parkir");
        edtDate.setText("12-12-2017 08:00");
    }

    private void tambahForum() {

        class TambahForum extends AsyncTask<String,String,String>{

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(TambahForumActivity.this);
                progressDialog.setMessage("Proses Menambah Posting");
                progressDialog.setCancelable(true);
                progressDialog.show();

                name = edtName.getText().toString();
                desc = edtDesc.getText().toString();
                date = edtDate.getText().toString();
            }

            @Override
            protected String doInBackground(String... params) {
                List<NameValuePair> param = new ArrayList<>();
                param.add(new BasicNameValuePair("name",name));
                param.add(new BasicNameValuePair("description",desc));
                param.add(new BasicNameValuePair("date",date));

                JSONObject jsonObject = jsonParser.makeHttpRequest(url_tambah_forum,"POST",param);

                try {
                    int sukses = jsonObject.getInt("success");
                    if (sukses == 1){
                        startActivity(new Intent(getApplicationContext(),ForumActivity.class));
                        Log.d("Berhasil", ""+sukses);
//                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(),"Tambah Posting Gagal", Toast.LENGTH_SHORT).show();
                        Log.d("Berhasil", "gagal");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("Berhasil", e.getMessage());
                }
                return null;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
                        finish();
            }
        }
        TambahForum tambahForum = new TambahForum();
        tambahForum.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home){
            Intent intent = new Intent(TambahForumActivity.this,ForumActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
