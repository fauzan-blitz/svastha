package com.health.healthassistant;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView tvMenu1, tvMenu2, tvMenu3,tvMenu4;
        tvMenu1 = findViewById(R.id.tv_menu1);
        tvMenu2 = findViewById(R.id.tv_menu2);
        tvMenu3 = findViewById(R.id.tv_menu3);
        tvMenu4 = findViewById(R.id.tv_menu4);

        tvMenu1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ExerciseActivity.class);
                startActivity(i);
            }
        });

        tvMenu2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), ForumActivity.class);
                startActivity(i);
            }
        });

        tvMenu3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FoodActivity.class);
                startActivity(i);
            }
        });

        tvMenu4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), BmiActivity.class);
                startActivity(i);
            }
        });

    }
}
