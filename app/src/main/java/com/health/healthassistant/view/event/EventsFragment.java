package com.health.healthassistant.view.event;


import com.health.healthassistant.R;
import com.health.healthassistant.adapter.EventMenuAdapter;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.Events;
import com.health.healthassistant.data.model.response.EventResponse;
import com.health.healthassistant.helper.CommonUtils;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsFragment extends Fragment {


    @BindView(R.id.rv_event)
    RecyclerView rvEvent;

    private EventMenuAdapter eventMenuAdapter;

    private ArrayList<Events> events;

    Api apiInterface;

    Unbinder unbinder;

    public EventsFragment() {
        // Required empty public constructor
    }

    public static EventsFragment newInstance() {
        EventsFragment fragment = new EventsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_events, container, false);
        apiInterface = ApiClient.getClient().create(Api.class);
        unbinder = ButterKnife.bind(this, view);
        initEvents();
        callAPI();
        initAction();
        return view;
    }

    private void initAction() {
        eventMenuAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), EventDetailActivity.class);
                intent.putExtra(BundleKey.KEY_CATEGORY_ID,
                    eventMenuAdapter.getDatas().get(position).getId());
                intent.putExtra(BundleKey.KEY_CATEGORY_NAME,
                    eventMenuAdapter.getDatas().get(position).getTitle());
                startActivity(intent);
            }
        });
    }

    private void callAPI() {
        Call<EventResponse> call = apiInterface
            .getEvent();
        call.enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                eventMenuAdapter.addOrUpdate(response.body().getUpcomingEvents());
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {

            }
        });
    }

    private void initEvents() {
        events = new ArrayList<>();
        eventMenuAdapter = new EventMenuAdapter(getContext(), events);
        rvEvent.setHasFixedSize(true);

        rvEvent.setLayoutManager(
            new LinearLayoutManager(getContext()));
        rvEvent.setAdapter(eventMenuAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
