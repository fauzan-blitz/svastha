package com.health.healthassistant.view.redeem;

import com.health.healthassistant.R;
import com.health.healthassistant.adapter.EventMenuAdapter;
import com.health.healthassistant.adapter.VoucherAdapter;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.Voucher;
import com.health.healthassistant.data.model.response.EventResponse;
import com.health.healthassistant.data.model.response.RedeemResponse;
import com.health.healthassistant.data.model.response.VoucherResponse;
import com.health.healthassistant.helper.CommonUtils;
import com.health.healthassistant.helper.MessageFactory;
import com.health.healthassistant.view.event.EventDetailActivity;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import github.nisrulz.screenshott.ScreenShott;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RedeemActivity extends AppCompatActivity implements VoucherAdapter.OnRedeemButtonClicked {

    Api apiInterface;

    SharedPreferences sharedPreferences;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_voucher)
    RecyclerView rvVoucher;

    private ArrayList<Voucher> vouchers;

    private VoucherAdapter voucherAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        apiInterface = ApiClient.getClient().create(Api.class);
        ActivityCompat
            .requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 00);
        initToolbar();
        initUI();
        callAPI();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Voucher List");
    }

    private void initUI() {
        vouchers = new ArrayList<>();
        voucherAdapter = new VoucherAdapter(RedeemActivity.this, vouchers);
        rvVoucher.setHasFixedSize(true);

        rvVoucher.setLayoutManager(
            new LinearLayoutManager(RedeemActivity.this));
        rvVoucher.setAdapter(voucherAdapter);
        voucherAdapter.setOnRedeemButtonClicked(this);
    }

    private void callAPI() {
        Call<VoucherResponse> call = apiInterface
            .getVoucher(sharedPreferences.getString(PrefreceKey.KEY_ID,""));
        call.enqueue(new Callback<VoucherResponse>() {
            @Override
            public void onResponse(Call<VoucherResponse> call, Response<VoucherResponse> response) {
                if (response.body().getVouchers().size() > 0)
                voucherAdapter.addOrUpdate(response.body().getVouchers());
            }

            @Override
            public void onFailure(Call<VoucherResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void redeemButtonListener(String voucherCode, int position, final String name) {
        Call<RedeemResponse> call = apiInterface
            .postRedeemVoucher(sharedPreferences.getString(PrefreceKey.KEY_ID,""),voucherCode);
        call.enqueue(new Callback<RedeemResponse>() {
            @Override
            public void onResponse(Call<RedeemResponse> call, Response<RedeemResponse> response) {
                if (response.body().getStatus() == 0) {
                    MessageFactory
                        .showAlertDialog(RedeemActivity.this, response.body().getMessage());
                } else {
                    MessageFactory.showVoucherCodeAlert(RedeemActivity.this,
                        "Nama Voucher",
                        name,
                        "Kode Voucher",
                        response.body().getVoucherCode(),
                        "Simpan Kode Voucher Untuk ditukar dengan promo yang berlaku.",
                        new MessageFactory.MyDialogActionListener() {
                            @Override
                            public void action() {

                            }

                            @Override
                            public void action(View v) {
                                captureScreen(v);
                            }
                        });
                }
            }

            @Override
            public void onFailure(Call<RedeemResponse> call, Throwable t) {
            }
        });
    }

    private void captureScreen(View view) {
//        Bitmap bitmap_rootview = ScreenShott.getInstance().takeScreenShotOfRootView(view);
        Bitmap bitmap_rootview = ScreenShott.getInstance().takeScreenShotOfView(view);

        try {
            ScreenShott.getInstance()
                .saveScreenshotToPicturesFolder(RedeemActivity.this, bitmap_rootview,
                    "svasthaSS");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
