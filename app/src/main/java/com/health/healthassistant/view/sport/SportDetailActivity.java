package com.health.healthassistant.view.sport;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BundleKey;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SportDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_feeds)
    ImageView ivFeeds;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_steps)
    TextView tvSteps;

    private String title, desc, id, img, categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport_detail);
        ButterKnife.bind(this);
        initIntent();
        initToolbar();
        initUI();

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(categoryName);
    }

    private void initUI() {
        tvTitle.setText(title);
        tvDate.setText(id);
        tvSteps.setText(desc);
        Glide
            .with(SportDetailActivity.this)
            .load(img)
            .apply(new RequestOptions()
                .placeholder(R.drawable.food_drinks_default_image))
            .into(ivFeeds);
    }

    private void initIntent() {
        title = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_TITLE);
        desc = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_DESC);
        id = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_ID);
        img = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_IMG);
        categoryName = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_NAME);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
