package com.health.healthassistant.view.feeds;

import com.health.healthassistant.R;
import com.health.healthassistant.adapter.BeverageAdapter;
import com.health.healthassistant.adapter.FeedsAdapter;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.Feed;
import com.health.healthassistant.data.model.entity.Beverage;
import com.health.healthassistant.data.model.entity.Food;
import com.health.healthassistant.data.model.response.FoodResponse;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeedsFragment extends Fragment {

    @BindView(R.id.rv_recommendation)
    RecyclerView rvRecommendation;

    Unbinder unbinder;

    @BindView(R.id.indicator_recommentdation)
    IndefinitePagerIndicator indicatorRecommentdation;

    @BindView(R.id.rv_recommendation_beverage)
    RecyclerView rvRecommendationBeverage;

    @BindView(R.id.indicator_recommentdation_beverage)
    IndefinitePagerIndicator indicatorRecommentdationBeverage;

    // TODO: Rename and change types of parameters
    private ArrayList<Food> feeds;

    private ArrayList<Food> beverages;

    private BeverageAdapter beverageAdapter;

    private FeedsAdapter feedsAdapter;

    private Api apiInterface;

    private SharedPreferences sharedPreferences;

    public FeedsFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static FeedsFragment newInstance() {
        FeedsFragment fragment = new FeedsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feeds, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(Api.class);
        sharedPreferences = getContext().getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        initView();
        initRecyclerView();
        return view;
    }

    private void initView() {
    }

    private void initRecyclerView() {
        initRecommendationFoodRecyclerView();
        initRecommendationBeverageRecyclerView();
    }

    private void initRecommendationFoodRecyclerView() {
        feeds = new ArrayList<>();
        feedsAdapter = new FeedsAdapter(getContext(), feeds);
        rvRecommendation.setHasFixedSize(true);
        rvRecommendation.setLayoutManager(
            new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvRecommendation.setAdapter(feedsAdapter);
        indicatorRecommentdation.attachToRecyclerView(rvRecommendation);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rvRecommendation);

        feedsAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), FoodDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleKey.KEY_CATEGORY_ID,
                    feedsAdapter.getDatas().get(position).getDiettype());
                bundle.putString(BundleKey.KEY_CATEGORY_TITLE,
                    feedsAdapter.getDatas().get(position).getName());
                bundle.putString(BundleKey.KEY_CATEGORY_DESC,
                    feedsAdapter.getDatas().get(position).getDescription());
                bundle.putString(BundleKey.KEY_CATEGORY_IMG,
                    feedsAdapter.getDatas().get(position).getImage());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        Call<FoodResponse> call = apiInterface
            .getRecommendationFood(sharedPreferences.getString(PrefreceKey.KEY_ID, ""));
        call.enqueue(new Callback<FoodResponse>() {
            @Override
            public void onResponse(Call<FoodResponse> call, Response<FoodResponse> response) {
                if (response.body().getFoods() != null)
                    feedsAdapter.addOrUpdate(response.body().getFoods());

            }

            @Override
            public void onFailure(Call<FoodResponse> call, Throwable t) {

            }
        });
    }

    private void initRecommendationBeverageRecyclerView() {
        beverages = new ArrayList<>();
        beverageAdapter = new BeverageAdapter(getContext(), beverages);
        rvRecommendationBeverage.setHasFixedSize(true);
        rvRecommendationBeverage.setLayoutManager(
            new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        rvRecommendationBeverage.setAdapter(beverageAdapter);
        indicatorRecommentdationBeverage.attachToRecyclerView(rvRecommendationBeverage);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rvRecommendationBeverage);

        beverageAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), FoodDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleKey.KEY_CATEGORY_ID,
                    beverageAdapter.getDatas().get(position).getDiettype());
                bundle.putString(BundleKey.KEY_CATEGORY_TITLE,
                    beverageAdapter.getDatas().get(position).getName());
                bundle.putString(BundleKey.KEY_CATEGORY_DESC,
                    beverageAdapter.getDatas().get(position).getDescription());
                bundle.putString(BundleKey.KEY_CATEGORY_IMG,
                    beverageAdapter.getDatas().get(position).getImage());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        Call<FoodResponse> call = apiInterface
            .getRecommendationBeverage(sharedPreferences.getString(PrefreceKey.KEY_ID, ""));
        call.enqueue(new Callback<FoodResponse>() {
            @Override
            public void onResponse(Call<FoodResponse> call, Response<FoodResponse> response) {
                if (response.body().getFoods() != null)
                    beverageAdapter.addOrUpdate(response.body().getFoods());

            }

            @Override
            public void onFailure(Call<FoodResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
}
