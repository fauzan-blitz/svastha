package com.health.healthassistant.view.sport;

import com.health.healthassistant.R;
import com.health.healthassistant.adapter.SportListAdapter;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.Sport;
import com.health.healthassistant.data.model.entity.SportList;
import com.health.healthassistant.data.model.response.ProfileResponse;
import com.health.healthassistant.data.model.response.SportResponse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SportActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.rv_sports)
    RecyclerView rvSports;

    private Api apiInterface;

    private SportListAdapter sportListAdapter;

    private ArrayList<SportList> sportLists;

    private int categoryId;

    private String categoryName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport);
        ButterKnife.bind(this);
        apiInterface = ApiClient.getClient().create(Api.class);
        initIntent();
        initToolbar();
        initSport();
        callAPI();
        initAction();
    }

    private void initAction() {
        sportListAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(SportActivity.this, SportDetailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString(BundleKey.KEY_CATEGORY_TITLE,
                    sportListAdapter.getDatas().get(position).getTitle());
                bundle.putString(BundleKey.KEY_CATEGORY_IMG,
                    sportListAdapter.getDatas().get(position).getImage());
                bundle.putString(BundleKey.KEY_CATEGORY_DESC,
                    sportListAdapter.getDatas().get(position).getDescription());
                bundle.putString(BundleKey.KEY_CATEGORY_ID,
                    sportListAdapter.getDatas().get(position).getId());
                bundle.putString(BundleKey.KEY_CATEGORY_NAME, categoryName);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    private void initIntent() {
        categoryId = getIntent().getExtras().getInt(BundleKey.KEY_CATEGORY_ID, 0);
        categoryName = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_NAME);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(categoryName);
    }

    private void callAPI() {
        Call<SportResponse> call = apiInterface
            .getSportByCategory(categoryId);
        call.enqueue(new Callback<SportResponse>() {
            @Override
            public void onResponse(Call<SportResponse> call, Response<SportResponse> response) {
                if (response.body().getSportLists() != null)
                    sportListAdapter.addOrUpdate(response.body().getSportLists());
            }

            @Override
            public void onFailure(Call<SportResponse> call, Throwable t) {

            }
        });
    }

    private void initSport() {
        sportLists = new ArrayList<>();
        sportListAdapter = new SportListAdapter(SportActivity.this, sportLists);
        rvSports.setHasFixedSize(true);

        sportListAdapter.addOrUpdate(sportLists);
        rvSports.setLayoutManager(
            new LinearLayoutManager(SportActivity.this));
        rvSports.setAdapter(sportListAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
