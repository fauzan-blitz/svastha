package com.health.healthassistant.view.profile;

import com.health.healthassistant.R;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.response.ProfileResponse;
import com.health.healthassistant.view.home.HomeActivity;
import com.health.healthassistant.view.login.LoginActivity;
import com.health.healthassistant.view.redeem.RedeemActivity;
import com.health.healthassistant.view.register.RegisterActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.btn_update_profile)
    LinearLayout btnUpdateProfile;

    @BindView(R.id.btn_reedem_point)
    LinearLayout btnReedemPoint;

    @BindView(R.id.btn_change_password)
    LinearLayout btnChangePassword;

    @BindView(R.id.btn_log_out)
    LinearLayout btnLogOut;

    @BindView(R.id.tv_name)
    TextView tvName;

    @BindView(R.id.tv_email)
    TextView tvEmail;

    @BindView(R.id.tv_points)
    TextView tvPoints;

    Api apiInterface;

    @BindView(R.id.tv_events)
    TextView tvEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        apiInterface = ApiClient.getClient().create(Api.class);

        setupToolbar();
        initUI();
        initAction();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getProfile();
    }

    private void getProfile() {
        Call<ProfileResponse> call = apiInterface
            .getProfile(sharedPreferences.getString(PrefreceKey.KEY_ID, ""));
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                String name = TextUtils.isEmpty(response.body().getFullname()) ? String
                    .format("%s %s", sharedPreferences.getString(PrefreceKey.KEY_NAME, ""),
                        sharedPreferences.getString(PrefreceKey.KEY_LAST_NAME, "")) : response
                    .body().getFullname();
                String email = TextUtils.isEmpty(response.body().getEmail()) ? sharedPreferences
                    .getString(PrefreceKey.KEY_EMAIL, "") : response.body().getEmail();
                tvName.setText(name);
                tvEmail.setText(email);
                tvPoints.setText(String.valueOf(response.body().getPoint()));
                tvEvents.setText(String.valueOf(response.body().getEvent()));
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Toast.makeText(ProfileActivity.this,t.getMessage(),Toast.LENGTH_SHORT);
            }
        });
    }

    private void initUI() {
        tvName.setText(String.format("%s %s", sharedPreferences.getString(PrefreceKey.KEY_NAME, ""),
            sharedPreferences.getString(PrefreceKey.KEY_LAST_NAME, "")));
        tvEmail.setText(sharedPreferences.getString(PrefreceKey.KEY_EMAIL, ""));
        tvPoints.setText(sharedPreferences.getString(PrefreceKey.KEY_POINT, ""));
    }

    private void initAction() {
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.clear();
                editor.apply();

                Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();

            }
        });

        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, RegisterActivity.class);
                intent.putExtra(BundleKey.KEY_IS_EDIT_REGISTER, true);
                startActivity(intent);
            }
        });

        btnReedemPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, RedeemActivity.class);
                startActivity(intent);
            }
        });
    }



    private void setupToolbar() {
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Profile");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(ProfileActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
