package com.health.healthassistant.view.home;

import com.health.healthassistant.BmiFragment;
import com.health.healthassistant.R;
import com.health.healthassistant.view.event.EventsFragment;
import com.health.healthassistant.view.feeds.FeedsFragment;
import com.health.healthassistant.view.profile.ProfileActivity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity{

    @BindView(R.id.iv_account)
    ImageView ivAccount;

    @BindView(R.id.iv_notif)
    ImageView ivNotif;

    @BindView(R.id.toolbar)
    RelativeLayout toolbar;

    @BindView(R.id.fl_container)
    FrameLayout flContainer;

    @BindView(R.id.tab)
    TabLayout tab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        replaceFragment(HomeFragment.newInstance());
        setupToolbar();
        setupTab();
        setupAction();

    }

    private void setupAction() {
        tab.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setTabActive(tab, true);
                setFragmentTransaction(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                setTabActive(tab, false);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ivAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    private void setFragmentTransaction(int tabPosition) {
        switch (tabPosition) {
            case 0:
                replaceFragment(HomeFragment.newInstance());
                break;
            case 1:
                replaceFragment(FeedsFragment.newInstance());
                break;
            case 2:
                replaceFragment(BmiFragment.newInstance());
                break;
            case 3:
                replaceFragment(EventsFragment.newInstance());
                break;
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fl_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    private void setTabActive(TabLayout.Tab tab, boolean isSelected) {
        int tabIconColor = isSelected ? ContextCompat.getColor(this, R.color.orange) :
            ContextCompat.getColor(this, R.color.grey_dot);
        tab.getIcon().setColorFilter(tabIconColor, PorterDuff.Mode.SRC_IN);
    }

    private void setupToolbar() {

    }

    private void setupTab() {

        tab.addTab(tab.newTab().setText("HOMES"));
        tab.addTab(tab.newTab().setText("FEEDS"));
        tab.addTab(tab.newTab().setText("BMI"));
        tab.addTab(tab.newTab().setText("EVENTS"));

        List<Integer> icons = new ArrayList<>();
        icons.add(R.drawable.ic_home);
        icons.add(R.drawable.ic_feed);
        icons.add(R.drawable.ic_bmi);
        icons.add(R.drawable.ic_event);

        tab.getTabAt(0).setIcon(icons.get(0));
        tab.getTabAt(1).setIcon(icons.get(1));
        tab.getTabAt(2).setIcon(icons.get(2));
        tab.getTabAt(3).setIcon(icons.get(3));
    }

}
