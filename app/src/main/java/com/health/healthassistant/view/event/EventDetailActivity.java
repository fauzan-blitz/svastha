package com.health.healthassistant.view.event;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.AppConst;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.Events;
import com.health.healthassistant.data.model.response.BaseResponse;
import com.health.healthassistant.data.model.response.EventDetailResponse;
import com.health.healthassistant.data.model.response.JoinEventResponse;
import com.health.healthassistant.helper.CommonUtils;
import com.health.healthassistant.helper.MessageFactory;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import github.nisrulz.screenshott.ScreenShott;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_feeds)
    ImageView ivFeeds;

    @BindView(R.id.tv_points)
    TextView tvPoints;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_date)
    TextView tvDate;

    @BindView(R.id.tv_location)
    TextView tvLocation;

    @BindView(R.id.tv_desc)
    TextView tvDesc;

    @BindView(R.id.btn_join)
    Button btnJoin;

    private String id, name;

    private Api apiInterface;

    private String title;

    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_detail);
        ButterKnife.bind(this);
        sharedPreferences = getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        apiInterface = ApiClient.getClient().create(Api.class);
        ActivityCompat
            .requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 00);
        initIntent();
        initToolbar();
        callAPI();
        initAction();
    }

    private void initAction() {
        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageFactory.showAlertDialog(EventDetailActivity.this, "Confirmation",
                    "Are you sure to join event " + title + " ?", "Sure",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callJoinEventAPI();
                        }
                    }, "Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
            }
        });
    }

    private void callJoinEventAPI() {
        Call<JoinEventResponse> call = apiInterface
            .postJoinEvent(sharedPreferences.getString(PrefreceKey.KEY_ID, ""), id);
        call.enqueue(new Callback<JoinEventResponse>() {
            @Override
            public void onResponse(Call<JoinEventResponse> call,
                Response<JoinEventResponse> response) {
                if (response.body().getStatus() == 0) {
                    MessageFactory
                        .showAlertDialog(EventDetailActivity.this, response.body().getMessage());
                } else {
                    MessageFactory.showVoucherCodeAlert(EventDetailActivity.this,
                        "Nama Event",
                        name,
                        "Id Registrasi",
                        response.body().getRegistrationId(),
                        "Simpan nomor registrasi ini dan tunjukan ketika menghadiri acara.",
                        new MessageFactory.MyDialogActionListener() {
                            @Override
                            public void action() {

                            }

                            @Override
                            public void action(View v) {
                                captureScreen(v);
                            }
                        });
                }
            }

            @Override
            public void onFailure(Call<JoinEventResponse> call, Throwable t) {
            }
        });
    }

    private void callAPI() {
        Call<EventDetailResponse> call = apiInterface.getEventDetail(id);
        call.enqueue(new Callback<EventDetailResponse>() {
            @Override
            public void onResponse(Call<EventDetailResponse> call,
                Response<EventDetailResponse> response) {
                initUI(response.body().getEvents());
            }

            @Override
            public void onFailure(Call<EventDetailResponse> call, Throwable t) {

            }
        });
    }

    private void initUI(Events body) {
        title = body.getTitle();
        tvTitle.setText(body.getTitle());
        tvPoints.setText(String.format("%s points", body.getRewardpoints()));
        tvDate.setText(body.getDate());
        tvLocation.setText(body.getPlace());
        tvDesc.setText(body.getDescription());
        Glide
            .with(EventDetailActivity.this)
            .load(body.getPicture())
            .apply(new RequestOptions()
                .placeholder(R.drawable.food_drinks_default_image))
            .into(ivFeeds);
    }

    private void initIntent() {
        id = getIntent().getStringExtra(BundleKey.KEY_CATEGORY_ID);
        name = getIntent().getStringExtra(BundleKey.KEY_CATEGORY_NAME);

    }

    private void initToolbar() {
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(name);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void captureScreen(View view) {
//        Bitmap bitmap_rootview = ScreenShott.getInstance().takeScreenShotOfRootView(view);
        Bitmap bitmap_rootview = ScreenShott.getInstance().takeScreenShotOfView(view);

        try {
            ScreenShott.getInstance()
                .saveScreenshotToPicturesFolder(EventDetailActivity.this, bitmap_rootview,
                    "svasthaSS");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
