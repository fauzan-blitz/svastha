package com.health.healthassistant.view.test;

import com.health.healthassistant.R;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TestActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.wv_tnc)
    WebView wvTnc;

    @BindView(R.id.pb_loading)
    ProgressBar pbLoading;

    private static final String LINK = "https://www.16personalities.com/free-personality-test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

        initToolbar();
        initView();
    }

    private void initView() {
        wvTnc.loadUrl(LINK);
        wvTnc.getSettings().setJavaScriptEnabled(true);
        wvTnc.setHorizontalScrollBarEnabled(false);
        wvTnc.setWebViewClient(new WebViewClient() {
            public void onPageFinished(WebView view, String url) {
                wvTnc.setVisibility(View.VISIBLE);
                pbLoading.setVisibility(View.GONE);
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Tes Kepribadian");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
