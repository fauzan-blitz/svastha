package com.health.healthassistant.view.register;

import com.health.healthassistant.R;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.UserEntity;
import com.health.healthassistant.data.model.request.UserRequest;
import com.health.healthassistant.data.model.response.BaseResponse;
import com.health.healthassistant.helper.CommonUtils;
import com.health.healthassistant.helper.ValidateUtils;
import com.health.healthassistant.view.login.LoginActivity;
import com.health.healthassistant.view.profile.ProfileActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.spn_gender)
    Spinner spnGender;

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.til_password_confirm)
    TextInputLayout tilPasswordConfirm;

    @BindView(R.id.til_phone)
    TextInputLayout tilPhone;

    @BindView(R.id.til_birhdate)
    TextInputLayout tilBirhdate;

    @BindView(R.id.btn_register)
    Button btnRegister;

    Api apiInterface;

    @BindView(R.id.til_first_name)
    TextInputLayout tilFirstName;

    @BindView(R.id.til_last_name)
    TextInputLayout tilLastName;

    SharedPreferences sharedPreferences;

    private boolean isEdit;
    private String userId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        sharedPreferences = getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        apiInterface = ApiClient.getClient().create(Api.class);
        initIntent();
        initAction();
    }

    private void initIntent() {
        isEdit = getIntent().getBooleanExtra(BundleKey.KEY_IS_EDIT_REGISTER, false);
        userId = sharedPreferences.getString(PrefreceKey.KEY_ID,"");
        if (isEdit) {
            tilFirstName.getEditText()
                .setText(sharedPreferences.getString(PrefreceKey.KEY_NAME, ""));
            tilLastName.getEditText()
                .setText(sharedPreferences.getString(PrefreceKey.KEY_LAST_NAME, ""));
            tilEmail.getEditText().setText(sharedPreferences.getString(PrefreceKey.KEY_EMAIL, ""));
            tilPhone.getEditText().setText(sharedPreferences.getString(PrefreceKey.KEY_PHONE, ""));
            tilBirhdate.getEditText()
                .setText(sharedPreferences.getString(PrefreceKey.KEY_BIRTHDATE, ""));

        }
        btnRegister.setText(isEdit? "Edit":"Register");
        tilPassword.setVisibility(isEdit ? View.GONE : View.VISIBLE);
        tilPasswordConfirm.setVisibility(isEdit ? View.GONE : View.VISIBLE);
    }

    private void initAction() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isEdit) {
                    validateDataForEdit();
                } else {
                    validateDataForRegist();
                }
            }
        });
    }

    private void validateDataForEdit() {
        if (ValidateUtils
            .validate("Field Required", tilFirstName, tilLastName, tilEmail,
                tilBirhdate, tilPhone)) {
            if (!CommonUtils
                .isEmailValid(tilEmail.getEditText().getText().toString())) {
                tilEmail.setError("Email Not Valid");
                return;
            }

            if (tilPhone.getEditText().getText().toString().length() < 11) {
                tilPhone.setError("Phone Invalid");
                return;
            }

            postEditProfile();
        }
    }

    private void postEditProfile() {
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName(tilFirstName.getEditText().getText().toString());
        userRequest.setEmail(tilEmail.getEditText().getText().toString());
        userRequest.setLastName(tilLastName.getEditText().getText().toString());
        userRequest.setGender(spnGender.getSelectedItem().toString());
        userRequest.setBirthdate(tilBirhdate.getEditText().getText().toString());
        userRequest.setPhone(tilPhone.getEditText().getText().toString());

        saveToUserPreference(userRequest);

        Call<BaseResponse> call = apiInterface
            .postEditProfile(userId,userRequest.getFirstName(), userRequest.getLastName(),
                userRequest.getEmail(), userRequest.getPhone(),
                userRequest.getGender(), userRequest.getBirthdate());
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                Context context = RegisterActivity.this;
                if (response.body().getStatus() == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View view = getLayoutInflater().inflate(R.layout.alert_order_made, null);
                    ImageView ivAlertIcon = (ImageView) view.findViewById(R.id.iv_alert_icon);
                    TextView tvAlertDesc = (TextView) view.findViewById(R.id.tv_alert_desc);
                    Button btnOk = (Button) view.findViewById(R.id.btn_alert_positive);
                    builder.setView(view);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    tvAlertDesc.setText("Edit Profile Success");
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            toProfileActivity();
                        }
                    });
                } else {
                    Toast.makeText(RegisterActivity.this, response.body().getMessage(),
                        Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }

    private void toProfileActivity() {
        Intent intent = new Intent(RegisterActivity.this, ProfileActivity.class);
        startActivity(intent);
        finish();
    }

    private void validateDataForRegist() {
        if (ValidateUtils
            .validate("Field Required", tilFirstName, tilLastName, tilEmail,
                tilPassword,
                tilPasswordConfirm, tilBirhdate, tilPhone)) {
            if (!CommonUtils
                .isEmailValid(tilEmail.getEditText().getText().toString())) {
                tilEmail.setError("Email Not Valid");
                return;
            }

            if (!CommonUtils
                .isPasswordStrongEnough(
                    tilPassword.getEditText().getText().toString())) {
                tilPassword.setError("Password Not Strong");
                return;
            }

            if (!CommonUtils
                .isConfirmPasswordValid(
                    tilPassword.getEditText().getText().toString(),
                    tilPasswordConfirm.getEditText().getText().toString())) {
                tilPasswordConfirm.setError("Password Not Match");
                return;
            }

            if (tilPhone.getEditText().getText().toString().length() < 11) {
                tilPhone.setError("Phone Invalid");
                return;
            }

            postRegister();
        }
    }

    private void postRegister() {
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName(tilFirstName.getEditText().getText().toString());
        userRequest.setEmail(tilEmail.getEditText().getText().toString());
        userRequest.setLastName(tilLastName.getEditText().getText().toString());
        userRequest.setGender(spnGender.getSelectedItem().toString());
        userRequest.setPassword(tilPasswordConfirm.getEditText().getText().toString());
        userRequest.setBirthdate(tilBirhdate.getEditText().getText().toString());
        userRequest.setPhone(tilPhone.getEditText().getText().toString());

        Call<BaseResponse> call = apiInterface
            .postRegister(userRequest.getFirstName(), userRequest.getLastName(),
                userRequest.getEmail(), userRequest.getPassword(), userRequest.getPhone(),
                userRequest.getGender(), userRequest.getBirthdate());
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                Context context = RegisterActivity.this;
                if (response.body().getStatus() == 1) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    View view = getLayoutInflater().inflate(R.layout.alert_order_made, null);
                    ImageView ivAlertIcon = (ImageView) view.findViewById(R.id.iv_alert_icon);
                    TextView tvAlertDesc = (TextView) view.findViewById(R.id.tv_alert_desc);
                    Button btnOk = (Button) view.findViewById(R.id.btn_alert_positive);
                    builder.setView(view);
                    final AlertDialog dialog = builder.create();
                    dialog.show();

                    tvAlertDesc.setText("Register Success");
                    btnOk.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            goToLogin();
                        }
                    });
                } else {
                    Toast.makeText(RegisterActivity.this, response.body().getMessage(),
                        Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }

    private void goToLogin() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveToUserPreference(
        UserRequest userEntity) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PrefreceKey.KEY_NAME, userEntity.getFirstName());
        editor.putString(PrefreceKey.KEY_LAST_NAME, userEntity.getLastName());
        editor.putString(PrefreceKey.KEY_EMAIL, userEntity.getEmail());
        editor.putString(PrefreceKey.KEY_GENDER, userEntity.getGender());
        editor.putString(PrefreceKey.KEY_BIRTHDATE, userEntity.getBirthdate());
        editor.putString(PrefreceKey.KEY_PHONE, userEntity.getPhone());
        editor.apply();
    }

}
