package com.health.healthassistant.view.splash;

import com.health.healthassistant.R;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.view.home.HomeActivity;
import com.health.healthassistant.view.login.LoginActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;

public class SplashActivity extends AppCompatActivity {
    SharedPreferences sharedPreferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        sharedPreferences = getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                decideNextPage();
            }
        };

//        wait 3 seconds
        handler.postDelayed(r, 2000);
    }

    private void decideNextPage() {
        if (TextUtils.isEmpty(sharedPreferences.getString(PrefreceKey.KEY_ID,""))){
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);

        } else {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            startActivity(intent);
        }
        finish();
    }
}
