package com.health.healthassistant.view.home;


import com.health.healthassistant.R;
import com.health.healthassistant.adapter.BannerAdapter;
import com.health.healthassistant.adapter.BeverageAdapter;
import com.health.healthassistant.adapter.EventAdapter;
import com.health.healthassistant.adapter.FeedsAdapter;
import com.health.healthassistant.adapter.MeditationMenuAdapter;
import com.health.healthassistant.adapter.SportAdapter;
import com.health.healthassistant.base.BaseRecyclerAdapter;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.Banner;
import com.health.healthassistant.data.model.entity.Beverage;
import com.health.healthassistant.data.model.entity.Events;
import com.health.healthassistant.data.model.entity.Feed;
import com.health.healthassistant.data.model.entity.Food;
import com.health.healthassistant.data.model.entity.MeditationMenu;
import com.health.healthassistant.data.model.entity.Sport;
import com.health.healthassistant.data.model.response.BannerResponse;
import com.health.healthassistant.data.model.response.EventResponse;
import com.health.healthassistant.data.model.response.FoodResponse;
import com.health.healthassistant.data.model.response.MeditationResponse;
import com.health.healthassistant.data.model.response.SportResponse;
import com.health.healthassistant.view.event.EventDetailActivity;
import com.health.healthassistant.view.feeds.FoodDetailActivity;
import com.health.healthassistant.view.meditation.MeditationDetailActivity;
import com.health.healthassistant.view.sport.SportActivity;
import com.health.healthassistant.view.test.TestActivity;
import com.rbrooks.indefinitepagerindicator.IndefinitePagerIndicator;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    @BindView(R.id.rv_banner)
    RecyclerView rvBanner;

    @BindView(R.id.indicator_recommentdation_beverage)
    IndefinitePagerIndicator indicatorRecommentdationBeverage;

    @BindView(R.id.tv_more_upcoming)
    TextView tvMoreUpcoming;

    @BindView(R.id.rv_upcoming_events)
    RecyclerView rvUpcomingEvents;

    @BindView(R.id.rv_recommendation)
    RecyclerView rvRecommendation;

    @BindView(R.id.indicator_recommentdation)
    IndefinitePagerIndicator indicatorRecommentdation;

    @BindView(R.id.rv_sports)
    RecyclerView rvSports;

    @BindView(R.id.rv_meditation)
    RecyclerView rvMeditation;

    @BindView(R.id.iv_meditation)
    ImageView ivMeditation;

    @BindView(R.id.cv_meditation)
    CardView cvMeditation;

    Unbinder unbinder;


    private ArrayList<Banner> banners;

    private BannerAdapter bannerAdapter;

    private ArrayList<Events> eventsArrayList;

    private EventAdapter eventAdapter;

    private ArrayList<Food> feeds;

    private FeedsAdapter feedsAdapter;

    private ArrayList<Sport> sports;

    private SportAdapter sportAdapter;

    private ArrayList<MeditationMenu> meditationMenus;

    private MeditationMenuAdapter meditationMenuAdapter;

    private Api apiInterface;

    private SharedPreferences sharedPreferences;


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        unbinder = ButterKnife.bind(this, view);
        apiInterface = ApiClient.getClient().create(Api.class);
        sharedPreferences = getContext().getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        initBanner();
        callUpcomingAPI();
        initRecommendationFoodRecyclerView();
        initSport();
        initMeditationMenu();
        initAction();
        return view;
    }

    private void callUpcomingAPI() {
        Call<EventResponse> call = apiInterface
            .getUpcomingEvents();
        call.enqueue(new Callback<EventResponse>() {
            @Override
            public void onResponse(Call<EventResponse> call, Response<EventResponse> response) {
                if (response.body().getUpcomingEvents() != null)
                    eventsArrayList = new ArrayList<>();
                eventAdapter = new EventAdapter(getContext(), eventsArrayList);
                rvUpcomingEvents.setHasFixedSize(true);
                rvUpcomingEvents.setLayoutManager(
                    new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                eventAdapter.addOrUpdate(response.body().getUpcomingEvents());
                rvUpcomingEvents.setAdapter(eventAdapter);

                eventAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getContext(), EventDetailActivity.class);
                        intent.putExtra(BundleKey.KEY_CATEGORY_ID,
                            eventAdapter.getDatas().get(position).getId());
                        intent.putExtra(BundleKey.KEY_CATEGORY_NAME,
                            eventAdapter.getDatas().get(position).getTitle());
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<EventResponse> call, Throwable t) {

            }
        });
    }

    private void initAction() {
        sportAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getContext(), SportActivity.class);
                Bundle bundle = new Bundle();
                bundle.putInt(BundleKey.KEY_CATEGORY_ID,
                    sportAdapter.getDatas().get(position).getId());
                bundle.putString(BundleKey.KEY_CATEGORY_NAME,
                    sportAdapter.getDatas().get(position).getName());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

        tvMoreUpcoming.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        ivMeditation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), TestActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initMeditationMenu() {

        Call<MeditationResponse> call = apiInterface
            .getMeditation();
        call.enqueue(new Callback<MeditationResponse>() {
            @Override
            public void onResponse(Call<MeditationResponse> call,
                Response<MeditationResponse> response) {
                meditationMenus = new ArrayList<>();
                meditationMenuAdapter = new MeditationMenuAdapter(getContext(), meditationMenus);
                rvMeditation.setHasFixedSize(true);

                meditationMenuAdapter.addOrUpdate(response.body().getMeditations());
                rvMeditation.setLayoutManager(
                    new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                rvMeditation.setAdapter(meditationMenuAdapter);

                meditationMenuAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getContext(), MeditationDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(BundleKey.KEY_CATEGORY_NAME,
                            meditationMenuAdapter.getDatas().get(position).getKategori());
                        bundle.putString(BundleKey.KEY_CATEGORY_TITLE,
                            meditationMenuAdapter.getDatas().get(position).getNamameditasi());
                        bundle.putString(BundleKey.KEY_CATEGORY_DESC,
                            meditationMenuAdapter.getDatas().get(position).getDeskripsi());
                        bundle.putString(BundleKey.KEY_CATEGORY_IMG,
                            meditationMenuAdapter.getDatas().get(position).getGambar());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<MeditationResponse> call, Throwable t) {

            }
        });
    }

    private void initBanner() {
        Call<BannerResponse> call = apiInterface.getBanner();
        call.enqueue(new Callback<BannerResponse>() {
            @Override
            public void onResponse(Call<BannerResponse> call, Response<BannerResponse> response) {
                banners = new ArrayList<>();
                bannerAdapter = new BannerAdapter(getContext(), banners);
                rvBanner.setHasFixedSize(true);

                bannerAdapter.addOrUpdate(response.body().getBanners());

                rvBanner.setLayoutManager(
                    new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                rvBanner.setAdapter(bannerAdapter);
                indicatorRecommentdationBeverage.attachToRecyclerView(rvBanner);
                SnapHelper snapHelper = new LinearSnapHelper();
                snapHelper.attachToRecyclerView(rvBanner);
            }

            @Override
            public void onFailure(Call<BannerResponse> call, Throwable t) {

            }
        });

    }

    private void initRecommendationFoodRecyclerView() {
        Call<FoodResponse> call = apiInterface
            .getRecommendationFood(sharedPreferences.getString(PrefreceKey.KEY_ID, ""));
        call.enqueue(new Callback<FoodResponse>() {
            @Override
            public void onResponse(Call<FoodResponse> call, Response<FoodResponse> response) {
                feeds = new ArrayList<>();
                feedsAdapter = new FeedsAdapter(getContext(), feeds);
                rvRecommendation.setHasFixedSize(true);

                feedsAdapter.addOrUpdate(response.body().getFoods());
                rvRecommendation.setLayoutManager(
                    new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                rvRecommendation.setAdapter(feedsAdapter);
                indicatorRecommentdation.attachToRecyclerView(rvRecommendation);
                SnapHelper snapHelper = new LinearSnapHelper();
                snapHelper.attachToRecyclerView(rvRecommendation);

                feedsAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getContext(), FoodDetailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(BundleKey.KEY_CATEGORY_ID,
                            feedsAdapter.getDatas().get(position).getDiettype());
                        bundle.putString(BundleKey.KEY_CATEGORY_TITLE,
                            feedsAdapter.getDatas().get(position).getName());
                        bundle.putString(BundleKey.KEY_CATEGORY_DESC,
                            feedsAdapter.getDatas().get(position).getDescription());
                        bundle.putString(BundleKey.KEY_CATEGORY_IMG,
                            feedsAdapter.getDatas().get(position).getImage());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<FoodResponse> call, Throwable t) {

            }
        });

    }

    private void initSport() {
        sports = new ArrayList<>();
        sportAdapter = new SportAdapter(getContext(), sports);
        rvSports.setHasFixedSize(true);
        sports.add(new Sport(1, "Kekuatan", R.drawable.excercise_icon_kekuatan));
        sports.add(new Sport(2, "Kecepatan", R.drawable.excercise_icon_kecepatan));
        sports.add(new Sport(3, "Kelenturan", R.drawable.excercise_icon_kelenturan));
        sports.add(new Sport(4, "Keseimbangan", R.drawable.excercise_icon_keseimbangan));
        sports.add(new Sport(5, "Kelincahan", R.drawable.excercise_icon_kelincahan));
        sports.add(new Sport(6, "Daya Tahan", R.drawable.excercise_icon_daya_tahan));

        sportAdapter.addOrUpdate(sports);
        rvSports.setLayoutManager(
            new GridLayoutManager(getContext(), 3));
        rvSports.setAdapter(sportAdapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
