package com.health.healthassistant.view.meditation;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.health.healthassistant.R;
import com.health.healthassistant.base.BundleKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.view.feeds.FoodDetailActivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MeditationDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_meditation)
    ImageView ivMeditation;

    @BindView(R.id.tv_title)
    TextView tvTitle;

    @BindView(R.id.tv_category)
    TextView tvCategory;

    @BindView(R.id.tv_desc)
    TextView tvDesc;

    private String title, desc, category, img;

    Api apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meditation_detail);
        ButterKnife.bind(this);
        apiInterface = ApiClient.getClient().create(Api.class);
        initIntent();
        initToolbar();
        initUI();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);


    }

    private void initIntent() {
        title = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_TITLE);
        desc = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_DESC);
        category = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_NAME);
        img = getIntent().getExtras().getString(BundleKey.KEY_CATEGORY_IMG);
    }

    private void initUI() {
        tvTitle.setText(title);
        tvCategory.setText(category);
        tvDesc.setText(desc);
        Glide
            .with(MeditationDetailActivity.this)
            .load(img)
            .apply(new RequestOptions()
                .placeholder(R.drawable.food_drinks_default_image))
            .into(ivMeditation);
    }
}
