package com.health.healthassistant.view.login;

import com.health.healthassistant.R;
import com.health.healthassistant.base.PrefreceKey;
import com.health.healthassistant.data.api.Api;
import com.health.healthassistant.data.api.ApiClient;
import com.health.healthassistant.data.model.entity.UserEntity;
import com.health.healthassistant.data.model.request.LoginRequest;
import com.health.healthassistant.data.model.response.LoginResponse;
import com.health.healthassistant.helper.CommonUtils;
import com.health.healthassistant.view.home.HomeActivity;
import com.health.healthassistant.view.register.RegisterActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.til_email)
    TextInputLayout tilEmail;

    @BindView(R.id.til_password)
    TextInputLayout tilPassword;

    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;

    @BindView(R.id.tv_register)
    TextView tvRegister;

    @BindView(R.id.btn_login)
    Button btnLogin;

    Api apiInterface;

    SharedPreferences sharedpreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        apiInterface = ApiClient.getClient().create(Api.class);
        sharedpreferences = getSharedPreferences(PrefreceKey.mypreference,
            Context.MODE_PRIVATE);
        initAction();
    }

    private void initAction() {
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = tilEmail.getEditText().getText().toString();
                String password = tilPassword.getEditText().getText().toString();
                if (TextUtils.isEmpty(email)) {
                    tilEmail.setError("Required");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    tilEmail.setError("Required");
                    return;
                }

                postLogin(email, password);
            }
        });
    }

    private void postLogin(String email, String password) {

        Call<LoginResponse> call = apiInterface.postLogin(email,password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.body().getStatus() == 1) {
                    saveToUserPreference(response.body().getData().get(0));
                    goToHome();
                } else {
                    Toast.makeText(LoginActivity.this, response.body().getMessage(),
                        Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtils.showLoadingDialog(LoginActivity.this,false);
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void saveToUserPreference(
        UserEntity userEntity) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(PrefreceKey.KEY_ID, userEntity.getId());
        editor.putString(PrefreceKey.KEY_NAME, userEntity.getFirstname());
        editor.putString(PrefreceKey.KEY_LAST_NAME, userEntity.getLastname());
        editor.putString(PrefreceKey.KEY_EMAIL, userEntity.getEmail());
        editor.putString(PrefreceKey.KEY_GENDER, userEntity.getGender());
        editor.putString(PrefreceKey.KEY_BIRTHDATE, userEntity.getBirthdate());
        editor.putString(PrefreceKey.KEY_PHONE, userEntity.getPhone());
        editor.putString(PrefreceKey.KEY_POINT, userEntity.getPoint());
        editor.apply();
    }

    private void goToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }
}
